#include "config.h"

namespace Config {
std::map<std::string, std::string> configuration;
bool configValid = false;
bool configInitialized = false;
std::string configPath = "configGUI.txt";
std::vector<std::string> configKeys = {"Autofill-IP", "Default-IP", "Autofill-Username", "Default-Username", "CLI-Path", "Keyfile-Path", "Use-SSL", "SSL-Path"};
} // namespace Config

void Config::setupDefaultConfig() {
	configuration.clear();
	for (std::string s : configKeys)
		configuration.insert(std::pair<std::string, std::string>(s, ""));
	setValue(configKeys[0], "0");
	setValue(configKeys[1], "0.0.0.0");
	setValue(configKeys[2], "0");
	setValue(configKeys[3], "user");
	setValue(configKeys[6], "0");
	configInitialized = true;
	configValid = true;
}

bool Config::checkConfig() {
	if (!configValid || configuration.size() != configKeys.size())
		return false;

	std::string autofill_ip = getValue("Autofill-IP");
	if (autofill_ip != "0" && autofill_ip != "1")
		return false;

	std::string autofill_user = getValue("Autofill-Username");
	if (autofill_user != "0" && autofill_user != "1")
		return false;

	std::string use_ssl = getValue("Use-SSL");
	if (use_ssl != "0" && use_ssl != "1")
		return false;

	return true;
}

bool Config::loadFile() {
	std::string line;

	if (!configInitialized)
		setupDefaultConfig();
	std::ifstream ifile(configPath);

	if (ifile.is_open()) {
		while (getline(ifile, line)) {
			std::stringstream ss(line);
			std::string segment;
			std::vector<std::string> v;
			while (getline(ss, segment, '=')) {
				v.push_back(segment);
			}
			if (v.size() != 2) {
				// One line doesn't have the format *=*
				configValid = false;
			} else {
				configuration[v.at(0)] = v.at(1);
			}
		}
		configValid = true;
		return true;
	}
	return false;
}

void Config::saveFile() {
	std::ofstream file;

	if (!configInitialized)
		setupDefaultConfig();

	file.open(configPath);
	for (auto const &x : configuration) {
		file << x.first << "=" << x.second << std::endl;
	}
	file.close();
}

std::string Config::getValue(const std::string &key) {
	auto it = configuration.find(key);
	if (it == configuration.end()) {
		return "";
	} else {
		return it->second;
	}
}

void Config::setValue(const std::string &key, const std::string &value) {
	auto it = configuration.find(key);

	if (it == configuration.end()) {
		configuration.insert(std::pair<std::string, std::string>(key, value));
	} else {
		it->second = value;
	}
}
