#ifndef CONFIG_H
#define CONFIG_H

#include <fstream>
#include <map>
#include <sstream>
#include <vector>

/**
 * Namespace which handles the GUI configuration and it's config file
 */
namespace Config {
/**
 * Set up the configuration map with the default values and declare the configuration valid
 */
void setupDefaultConfig();
/**
 * Check the loaded config if the values are valid and if it's size is valid
 * @return Is the config valid?
 */
bool checkConfig();
/**
 * Load the configuration file - if values are missing, they are added with their default values
 * @return Has the config file been successfully loaded?
 */
bool loadFile();
/**
 * Save the configuration map to the config file
 */
void saveFile();
/**
 * Get a value from the configuration map
 * @param key The configuration key to get
 * @return The value
 */
std::string getValue(const std::string &key);
/**
 * Set a value in the configuration map to the given one
 * @param key The configuration key
 * @param value The new value
 */
void setValue(const std::string &key, const std::string &value);
} // namespace Config

#endif // CONFIG_H
