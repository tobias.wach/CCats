#ifndef TCPAPPENDCHANNEL_H
#define TCPAPPENDCHANNEL_H

#include "../BidirectionalChannels.hpp"

/**
 * @class TCPAppendChannel
 *
 * A CovertChannel which appends data to the TCP payload
 *
 * @param N number of bytes which can be used to transmit data
 * @param PASSIVE true - server only reacts to incoming channel | false - server initiates channel
 */
template <int N, bool PASSIVE> class TCPAppendChannel : public BidirectionalChannels<N, PASSIVE> {
public:
	/**
	 * Sets up a CovertChannel.
	 *
	 * Creates a CovertChannel, sets the network interfaces for sniffing and sending and sets the filter.
	 *
	 * @param innerInterface name of the interface of the inner network
	 * @param outerInterface name of the interface of the outer network
	 * @param targetIP IP of the target server
	 * @param targetPort Port of the target server
	 */
	TCPAppendChannel(const std::string &innerInterface, const std::string &outerInterface, const std::string &targetIP, const std::string &targetPort)
	    : BidirectionalChannels<N, PASSIVE>(innerInterface, outerInterface, targetIP, targetPort) {}

	/**
	 * Destroys the CovertChannel.
	 */
	virtual ~TCPAppendChannel() {}

protected:
	/**
	 * Handler for sniffed packets filterd to forward from the outer network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromOuter(Tins::PDU &pdu) {
		Tins::TCP &tcp = pdu.rfind_pdu<Tins::TCP>();

		// get payload
		Tins::RawPDU *raw = tcp.find_pdu<Tins::RawPDU>();
		if (raw != nullptr) {
			Tins::RawPDU::payload_type &payload = raw->payload();

			// read data from payload
			std::size_t size = payload.size();

			if (size < N) {
				std::cerr << __PRETTY_FUNCTION__ << " payload size is too small" << std::endl;
			} else {
				uint8_t *data = &payload.front();
				data += size - N;

				BidirectionalChannels<N, PASSIVE>::protocol.receive(data);

				// resize payload
				payload.resize(size - N);
			}
		}

		BidirectionalChannels<N, PASSIVE>::innerSender.send(pdu);

		return true;
	}

	/**
	 * Handler for sniffed packets filterd to forward from the inner network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromInner(Tins::PDU &pdu) {
		Tins::TCP &tcp = pdu.rfind_pdu<Tins::TCP>();

		// get payload
		Tins::RawPDU *raw = tcp.find_pdu<Tins::RawPDU>();
		if (raw != nullptr) {
			Tins::RawPDU::payload_type &payload = raw->payload();

			// resize payload
			std::size_t size = payload.size();
			payload.resize(size + N);

			// write data in payload
			uint8_t *data = &payload.front();
			data += size;

			BidirectionalChannels<N, PASSIVE>::protocol.send(data);
		}

		BidirectionalChannels<N, PASSIVE>::outerSender.send(pdu);

		return true;
	}
};

#endif
