import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Dialogs 1.0

Page {
    width: 1280
    height: 570
    id: settingsForm

    font.capitalization: Font.MixedCase

    Connections {
        target: _qmlHandler
        onCloseWindow: {
            window.close()
        }
        onLoadSettings: {
            settingsSaveIpSwitch.checked = saveIP
            settingsSaveUsernameSwitch.checked = saveUsername
            settingsCliPath.text = "CLI-Path:   " + cliPath
            settingsKeyPath.text = "            " + keyPath
        }
        onKeyfileStatus: {
            if (success) {
                settingsKeyStatus.text = "Keyfile: OK"
                settingsKeyStatus.color = "#00ad11"
            } else {
                settingsKeyStatus.text = "Keyfile: Error"
                settingsKeyStatus.color = "#df3f3f"
                settingsKeyPath.text = msg
            }
        }
        onKeyfileClosedOK: {
            settingsKeyStatus.text = "Keyfile:"
            settingsKeyStatus.color = "#000000"
            settingsKeyPath.text = "   "
        }
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 400
            Layout.bottomMargin: 20

            ColumnLayout {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 500
                Layout.preferredHeight: parent.height

                Text {
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: 400
                    Layout.preferredHeight: 50

                    text: "Autofill default IP on start:"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 20
                }

                Text {
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: 400
                    Layout.preferredHeight: 50

                    text: "Autofill default username on start:"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 20
                }

                Text {
                    id: settingsCliPath
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: 400
                    Layout.preferredHeight: 50

                    text: "CLI-Path:   "
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 20
                }

                Text {
                    id: settingsKeyStatus
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: 400
                    Layout.preferredHeight: 50

                    text: "Keyfile:"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 20
                }

                Text {
                    id: settingsKeyPath
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: 400
                    Layout.preferredHeight: 50

                    text: "   "
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 20
                }

                Text {
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: 400
                    Layout.preferredHeight: 50

                    text: "Delete my account:"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: 20
                }
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 500
                Layout.preferredHeight: parent.height

                Switch {
                    id: settingsSaveIpSwitch
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredHeight: 50
                    Layout.preferredWidth: 400
                    text: ""
                    checked: false
                    display: AbstractButton.IconOnly
                }

                Switch {
                    id: settingsSaveUsernameSwitch
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredHeight: 50
                    Layout.preferredWidth: 400
                    text: ""
                    checked: false
                    display: AbstractButton.IconOnly
                }

                Button {
                    id: settingsChangeCliPathButton
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredHeight: 50
                    Layout.preferredWidth: 220
                    text: "Change (req. restart)"
                    font.pixelSize: 20
                    // @disable-check M223
                    onClicked: {
                        // @disable-check M222
                        settingsCliDialog.open()
                    }
                }

                ColumnLayout {
                    Layout.alignment: Qt.AlignCenter
                    Button {
                        id: settingsChangeKeyfilePathButton
                        Layout.alignment: Qt.AlignCenter
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 220
                        text: "Select"
                        font.pixelSize: 20
                        // @disable-check M223
                        onClicked: {
                            // @disable-check M222
                            settingsKeyfileDialog.open()
                        }
                    }

                    Button {
                        id: settingsDisableKeyfile
                        Layout.alignment: Qt.AlignCenter
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 220
                        text: "Close Keyfile"
                        font.pixelSize: 20
                        // @disable-check M223
                        onClicked: {
                            // @disable-check M222
                            _qmlHandler.onKeyfileClosed()
                        }
                    }
                }

                Button {
                    id: settingsDeleteMeButton
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredHeight: 50
                    Layout.preferredWidth: 150
                    text: "Delete Me"
                    font.pixelSize: 20
                    // @disable-check M223
                    onClicked: {
                        // @disable-check M222
                        deleteMePopup.open()
                    }
                }
            }
        }

        Rectangle {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 2
            color: Material.accent
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50
            Layout.bottomMargin: 20

            Button {
                id: settingsResetButton
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 200
                Layout.preferredHeight: 50
                text: "Select defaults"
                font.pixelSize: 20
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onSettingsResetButton()
                }
            }

            Button {
                id: settingsSaveButton
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 200
                Layout.preferredHeight: 50
                text: "Save Changes"
                font.pixelSize: 20
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onSettingsSaveButton(
                                settingsSaveIpSwitch.checked,
                                settingsSaveUsernameSwitch.checked,
                                // @disable-check M222
                                settingsCliPath.text.replace("CLI-Path:   ",
                                                             ""),
                                // @disable-check M222
                                settingsKeyPath.text.replace("   ", ""))
                }
            }

            Button {
                id: settingsRevertChangesButton
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 200
                Layout.preferredHeight: 50
                text: "Revert Changes"
                font.pixelSize: 20
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onSettingsRevertChangesButton()
                }
            }
        }
    }

    FileDialog {
        id: settingsCliDialog
        nameFilters: ["CLI file (ccats-cli)"]
        title: "Please select the CLI File"
        folder: shortcuts.home
        // @disable-check M223
        onAccepted: {
            var path = settingsCliDialog.fileUrl.toString()
            // @disable-check M222
            path = path.replace(/^(file:\/{2})/, "")
            settingsCliPath.text = "CLI-Path:   " + path
        }
    }

    FileDialog {
        id: settingsKeyfileDialog
        title: "Select keyfile to use"
        folder: shortcuts.home
        // @disable-check M223
        onAccepted: {
            var path = settingsKeyfileDialog.fileUrl.toString()
            // @disable-check M222
            path = path.replace(/^(file:\/{2})/, "")
            settingsKeyPath.text = "   " + path
            // @disable-check M222
            _qmlHandler.onKeyfileSelected(path)
        }
    }

    DeleteMePopup {
        id: deleteMePopup
    }
}
