#ifndef BIDIRECTIONALCHANNELS_H
#define BIDIRECTIONALCHANNELS_H

#include "CovertChannel.h"
#include "Protocols/CovertProtocolBidirectional.hpp"

/**
 * @class BidirectionalChannels
 *
 * Abstract class which implements the methods and constructors which are equal in all bidirectional channels.
 *
 * @param N number of bytes which can be used to transmit data
 * @param PASSIVE true - server only reacts to incoming channel | false - server initiates channel
 */
template <int N, bool PASSIVE> class BidirectionalChannels : public CovertChannel {
public:
	/**
	 * Sets up a CovertChannel.
	 *
	 * Creates a CovertChannel, sets the network interfaces for sniffing and sending and sets the filter.
	 *
	 * @param innerInterface name of the interface of the inner network
	 * @param outerInterface name of the interface of the outer network
	 * @param targetIP IP of the target server
	 * @param targetPort Port of the target server
	 */
	BidirectionalChannels(const std::string &innerInterface, const std::string &outerInterface, const std::string &targetIP, const std::string &targetPort)
	    : CovertChannel(innerInterface, outerInterface, "not (tcp and host " + targetIP + " and port " + targetPort + ")",
	                    "not (tcp and host " + targetIP + " and port " + targetPort + ")", "tcp and host " + targetIP + " and port " + targetPort,
	                    "tcp and host " + targetIP + " and port " + targetPort) {}

	/**
	 * Destroys the CovertChannel.
	 */
	virtual ~BidirectionalChannels() {}

	/* ChannelControls */

	/**
	 * Starts sending a file.
	 *
	 * Starts sending a file if no transmission is running and the file exists.
	 *
	 * @param fileName name of the file in the file directory
	 * @return true - file will be sent | false - file was not accepted
	 */
	virtual bool sendFile(const std::string &fileName) { return protocol.sendFile(fileName); }

	/**
	 * Get the progress
	 *
	 * @return progress counters
	 */
	virtual std::pair<uint32_t, uint32_t> getProgress() { return protocol.getProgress(); }

	/**
	 * Test if a transfer is running
	 *
	 * @return true - a transfer runs | false - no transfer runs
	 */
	virtual bool isTransferRunning() { return protocol.isTransferRunning(); }

	/**
	 * Resets state and sets reset flag so a reset signal is sent in the next packet
	 */
	virtual void reset() { protocol.reset(); }

	/**
	 * Test if a transfer is running
	 *
	 * @return true - a transfer runs | false - no transfer runs
	 */
	virtual std::time_t getTransferStart() { return protocol.getTransferStart(); }

	/**
	 * Get file name of the file which is currently be sent or received.
	 *
	 * @return file name
	 */
	virtual std::string getFileName() { return protocol.getFileName(); };

	/* =============== */

protected:
	/**
	 * protocol used to transmit data
	 */
	CovertProtocolBidirectional<N, PASSIVE> protocol;
};

#endif
