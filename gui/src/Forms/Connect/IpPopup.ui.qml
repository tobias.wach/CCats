import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Popup {
    id: popup
    height: 250
    dim: true
    clip: false
    width: 400
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    signal resetOnClose

    onClosed: {
        resetOnClose()
    }

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    Connections {
        target: _qmlHandler
        onIpPopupSetIP: {
            ipPopupIpInput.text = default_ip
            ipPopupConnectButton.enabled = ipPopupIpInput.acceptableInput
        }
        onIpPopupClose: {
            popup.close()
        }
        onIpPopupOpen: {
            popup.open()
        }
        onIpPopupSetStatus: {
            ipPopupStatusText.text = status
        }
        onIpPopupEnableConnectButton: {
            ipPopupConnectButton.enabled = true
        }
        onIpPopupDisableConnectButton: {
            ipPopupConnectButton.enabled = false
        }
        onIpPopupCheckSaveCheckbox: {
            ipPopupSetDefaultCheckbox.checked = true
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            id: ipPopupText
            text: qsTr("Enter the IP to connect:")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        TextField {
            Layout.alignment: Qt.AlignCenter
            id: ipPopupIpInput
            selectByMouse: true
            focus: true
            text: qsTr("")
            placeholderText: "IP-Address"
            horizontalAlignment: Text.AlignHCenter
            validator: RegExpValidator {
                regExp: /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:[0-9]+)?$/
            }
            // @disable-check M222
            Keys.onReturnPressed: ipPopupConnectButton.activate()
            // @disable-check M222
            Keys.onEnterPressed: ipPopupConnectButton.activate()

            onTextEdited: ipPopupConnectButton.enabled = ipPopupIpInput.acceptableInput

            Connections {
                target: popup
                onResetOnClose: ipPopupIpInput.text = ""
            }
        }

        CheckBox {
            id: ipPopupSetDefaultCheckbox
            Layout.alignment: Qt.AlignCenter
            checked: false
            text: "Save as default IP"
        }

        Text {
            id: ipPopupStatusText
            color: "#df3f3f"
            text: qsTr("")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.alignment: Qt.AlignCenter
            font.pixelSize: 20
            wrapMode: Text.WordWrap
            Layout.preferredWidth: parent.width

            Connections {
                target: popup
                onResetOnClose: ipPopupStatusText.text = ""
            }
        }

        Button {
            Layout.alignment: Qt.AlignCenter
            id: ipPopupConnectButton
            text: qsTr("Connect")
            rightPadding: 8
            padding: 12
            enabled: false
            font.pointSize: 16
            // @disable-check M223
            onClicked: {
                // @disable-check M222
                ipPopupConnectButton.activate()
            }

            // @disable-check M222
            function activate() {
                // @disable-check M223
                if (ipPopupConnectButton.enabled) {
                    // @disable-check M222
                    _qmlHandler.onIpPopupConnectButton(
                                ipPopupIpInput.text,
                                ipPopupSetDefaultCheckbox.checked)
                }
            }
        }
    }
}
