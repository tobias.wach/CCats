#ifndef TCPOPTIONTIMESTAMPCHANNEL_H
#define TCPOPTIONTIMESTAMPCHANNEL_H

#include "../BidirectionalChannels.hpp"

#include <utility>

/**
 * @class TCPOptionTimestampChannel
 *
 * A CovertChannel which hides data in the TCP timestamp option field.
 *
 * @warning Only use on connections which will never use the timestamp option on their own!!!
 *
 * @param PASSIVE true - server only reacts to incoming channel | false - server initiates channel
 */
template <bool PASSIVE> class TCPOptionTimestampChannel : public BidirectionalChannels<8, PASSIVE> {
public:
	/**
	 * Sets up a CovertChannel.
	 *
	 * Creates a CovertChannel, sets the network interfaces for sniffing and sending and sets the filter.
	 *
	 * @param innerInterface name of the interface of the inner network
	 * @param outerInterface name of the interface of the outer network
	 * @param targetIP IP of the target server
	 * @param targetPort Port of the target server
	 */
	TCPOptionTimestampChannel(const std::string &innerInterface, const std::string &outerInterface, const std::string &targetIP, const std::string &targetPort)
	    : BidirectionalChannels<8, PASSIVE>(innerInterface, outerInterface, targetIP, targetPort) {}

	/**
	 * Destroys the CovertChannel.
	 */
	virtual ~TCPOptionTimestampChannel() {}

protected:
	/**
	 * Handler for sniffed packets filterd to forward from the outer network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromOuter(Tins::PDU &pdu) {
		Tins::TCP &tcp = pdu.rfind_pdu<Tins::TCP>();

		std::pair<uint32_t, uint32_t> timestamp = tcp.timestamp();
		uint64_t data = ((uint64_t)timestamp.first) << 32 | timestamp.second;
		BidirectionalChannels<8, PASSIVE>::protocol.receive((uint8_t *)(&data));
		tcp.remove_option(Tins::TCP::OptionTypes::TSOPT);
		BidirectionalChannels<8, PASSIVE>::innerSender.send(pdu);

		return true;
	}

	/**
	 * Handler for sniffed packets filterd to forward from the inner network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromInner(Tins::PDU &pdu) {
		Tins::TCP &tcp = pdu.rfind_pdu<Tins::TCP>();

		uint64_t data = 0;
		BidirectionalChannels<8, PASSIVE>::protocol.send((uint8_t *)(&data));
		tcp.timestamp(data >> 32, data);
		BidirectionalChannels<8, PASSIVE>::outerSender.send(pdu);

		return true;
	}
};

#endif
