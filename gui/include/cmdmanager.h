#ifndef CMDMANAGER_H
#define CMDMANAGER_H

#include "qmlhandler.h"

#include <json/json.h>

#include <map>
#include <string>
#include <vector>

/**
 * The struct for a file entry in the GUI file list
 */
struct fileEntry {
	bool dirty;
	std::string type;
	std::string method;
	int progress;
	float speed;
};

/**
 * Namespace which handles the commands received from the CLI
 */
namespace CmdManager {
/**
 * Initialize the cmdmap with the references to the according methods
 */
void init();
/**
 * Set the qml handler pointer
 * @param q The reference to the qml handler
 */
void setQmlHandler(QMLHandler *q);
/**
 * Update a file in the file map
 * @param name The name of the file
 * @param type The new type of the file
 * @param method The new method of the file
 * @param progress The new progress of the file
 * @param speed The new speed of the file
 */
void updateInternalFile(std::string name, std::string type, std::string method, int progress, float speed);
/**
 * Update the GUI file list to match the file map
 */
void emitFileList();
/**
 * Clear the file map
 */
void cleanInternalList();
/**
 * Execute a command received from the CLI
 * @param cmd The command to be executed using the cmdmap
 * @param root The entire JSON value
 */
void executeCmd(std::string cmd, Json::Value root);
/**
 * Sets the cached ip address
 * @param ip The ip address
 */
void setCachedIP(QString ip);
/**
 * Sets the cached port
 * @param port The port
 */
void setCachedPort(QString port);

/**
 * Handle the error command
 *
 * Print the error in the log
 * @param root The entire JSON value
 */
void handleError(Json::Value root);
/**
 * Handle the status command
 *
 * Update the status text in the GUI
 * @param root The entire JSON value
 */
void handleStatus(Json::Value root);
/**
 * Handle the close command
 *
 * Set the program as inactive to stop the CLI background threads
 * @param root The entire JSON value
 */
void handleClose(Json::Value root);
/**
 * Handle the list command
 *
 * List the server files in the GUI
 * @param root The entire JSON value
 */
void handleList(Json::Value root);
/**
 * Handle the extendedlist command
 *
 * List the server files in the GUI with detailed information
 * @param root The entire JSON value
 */
void handleExtendedList(Json::Value root);
/**
 * Handle the connect command
 *
 * If there was an error, reactivate the connect button and show the error
 * @param root The entire JSON value
 */
void handleConnect(Json::Value root);
/**
 * Handle the version command
 *
 * Close the ip popup and open the login/signup popup if it was accepted, else reactivate the connect button and show the error
 * @param root The entire JSON value
 */
void handleVersion(Json::Value root);
/**
 * Handle the login command
 *
 * Set the loggedin value to true and close the popup if it was accepted, else reactivate the login button and show the error
 * @param root The entire JSON value
 */
void handleLogin(Json::Value root);
/**
 * Handle the signup command
 *
 * Set the loggedin value to true and close the popup if it was accepted, else reactivate the signup button and show the error
 * @param root The entire JSON value
 */
void handleSignup(Json::Value root);
/**
 * Handle the put command
 *
 * Show an error if it was not accepted
 * @param root The entire JSON value
 */
void handlePut(Json::Value root);
/**
 * Handle the putdata command
 *
 * Show an error if it was not accepted
 * @param root The entire JSON value
 */
void handlePutData(Json::Value root);
/**
 * Handle the get command
 *
 * Show an error if it was not accepted, else disable the file's download button
 * @param root The entire JSON value
 */
void handleGet(Json::Value root);
/**
 * Handle the getdata command
 *
 * Show an error if it was not accepted
 * @param root The entire JSON value
 */
void handleGetData(Json::Value root);
/**
 * Handle the deleteme command
 *
 * Show an error if it was not accepted, else disconnect from the server and restart the application
 * @param root The entire JSON value
 */
void handleDeleteMe(Json::Value root);
/**
 * Handle the deletefile command
 *
 * Show an error if it was not accepted, else show the confirmation in the log
 * @param root The entire JSON value
 */
void handleDeleteFile(Json::Value root);
/**
 * Handle the notifications command
 *
 * Show an error if it was not accepted, else handle the new notifications if there are some
 * @param root The entire JSON value
 */
void handleNotifications(Json::Value root);
/**
 * Handle the queue command
 *
 * Show an error if it was not accepted
 * @param root The entire JSON value
 */
void handleQueue(Json::Value root);
/**
 * Handle the dequeue command
 *
 * Show an error if it was not accepted
 * @param root The entire JSON value
 */
void handleDequeue(Json::Value root);
/**
 * Handle the extendedstatus command
 *
 * Show an error if it was not accepted, else update the file list with the new data
 * @param root The entire JSON value
 */
void handleExtendedStatus(Json::Value root);
/**
 * Handle the keyfile command
 *
 * Show an error if it was not accepted, else update the keyfile status in the settings tab
 * @param root The entire JSON value
 */
void handleKeyfile(Json::Value root);
/**
 * Handle the closekey command
 *
 * Show an error if it was not accepted, else update the keyfile status in the settings tab
 * @param root The entire JSON value
 */
void handleClosekey(Json::Value root);
/**
 * Handle the disconnect command
 *
 * Set the loggedin variable to false to stop the background CLI threads if it was accepted
 * @param root The entire JSON value
 */
void handleDisconnect(Json::Value root);
/**
 * Handle the connectionerror command
 *
 * Sets the footer error to encourage the user to restart the application on a lost connection
 * @param root The entire JSON value
 */
void handleConnectionError(Json::Value root);
} // namespace CmdManager

#endif // CMDMANAGER_H
