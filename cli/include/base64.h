#ifndef BASE64_H
#define BASE64_H

#include <string>
#include <vector>

namespace base64 {
/**
 * Decodes base64 encoded strings.
 *
 * @param val base64 encoded string
 *
 * @return normal string
 */
std::string decode(const std::string &val);

/**
 * Decodes base64 encoded strings.
 *
 * @param val base64 encoded string
 *
 * @return char vector
 */
std::vector<char> decodeVector(const std::string &val);

/**
 * Encodes a string to base64.
 *
 * @param val normal string
 *
 * @return base64 encoded string
 */
std::string encode(const std::string &val);

/**
 * Encodes a vector to base64.
 *
 * @param val char vector
 *
 * @return base64 encoded string
 */
std::string encodeVector(const std::vector<char> &val);
} // namespace base64

#endif
