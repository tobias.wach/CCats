#ifndef CHANNELCONTROLS_H
#define CHANNELCONTROLS_H

#include <ctime>
#include <string>
#include <utility>

class ChannelControls {
public:
	/**
	 * Send a file over the covert channel.
	 *
	 * @param fileName name of the file in the file directory
	 * @return true - file will be sent | false - file was not accepted
	 */
	virtual bool sendFile(const std::string &fileName) = 0;

	/**
	 * Get the progress
	 *
	 * @return progress counters
	 */
	virtual std::pair<uint32_t, uint32_t> getProgress() = 0;

	/**
	 * Get the transfer start time
	 *
	 * @return start time of the transfer
	 */
	virtual std::time_t getTransferStart() = 0;

	/**
	 * Test if a transfer is running
	 *
	 * @return true - a transfer runs | false - no transfer runs
	 */
	virtual bool isTransferRunning() = 0;

	/**
	 * Resets the state of the channel
	 */
	virtual void reset() = 0;

	/**
	 * Get file name of the file which is currently be sent or received.
	 *
	 * @return file name
	 */
	virtual std::string getFileName() = 0;

protected:
	/**
	 * Time when the transfer was started with sendFile
	 */
	std::time_t transferStart;

	/**
	 * file name of the file wich is being sent or received
	 */
	std::string fileName;
};

#endif
