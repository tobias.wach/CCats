#ifndef CONFIG_H
#define CONFIG_H

#include <fstream>
#include <map>
#include <sstream>
#include <vector>

/**
 * Namespace which loads and holds configuration data
 */
namespace Config {

/**
 * Load data from file into storage
 *
 * @param filename the name of the file which holds the data
 */
bool init(const std::string &filename);

/**
 * Load data from file into storage
 *
 * @param key get value from storage by key
 */
std::string getValue(const std::string &key);

/**
 * Map which holds the configuration data
 */
extern std::map<std::string, std::string> storage;
}; // namespace Config

#endif
