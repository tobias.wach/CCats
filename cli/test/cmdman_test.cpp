#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../include/cmdman.h"
#include "../include/global.h"
#include "fileman_mock.h"

#include "CmdManForTest.cpp"

/* template

TEST(testVersion, Positive) {
        FileManMock fm;
        CmdManForTest cm(fm, dummyDebugPrint);

        std::string cmd;
        std::vector<std::string> args;
        Json::Value root;
        CmdMan::CmdRet retval;

        // prepare root/cmd/args
        cmd = "";
        args = {"", ""};
        root[""] = "";

        // stick into cmdman
        cm.execute(cmd, args);
        cm.handle(root);

        // check things
        EXPECT_EQ(retval.type, sometypes);
        EXPECT_EQ(retval.nextcommand, somestring);
        EXPECT_TRUE(retval.msg[""].asBool());
        EXPECT_FALSE(retval.msg[""].asBool());
        ON_CALL(fm, methodname()).WillByDefault(testing::Return(true));
}

*/

/* holds all tests */
namespace {

void dummyDebugPrint(string x) {
	// do nothing
}

/*
 * =====================================
 * tests for connect
 */
TEST(testConnect, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "connect";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testConnect, Positive_NoPortSpecified) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "connect";
	args = {"1.222.33.4"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	cm.stateSetConnectionOk();

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::connect);
	EXPECT_EQ(retvalCmd.msg["address"].asString(), "1.222.33.4");
	EXPECT_EQ(retvalCmd.msg["port"].asString(), "1234");

	EXPECT_TRUE(cm.isConnected());
}

TEST(testConnect, Positive_PortSpecified) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "connect";
	args = {"1.222.33.4", "1337"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	cm.stateSetConnectionOk();

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::connect);
	EXPECT_EQ(retvalCmd.msg["address"].asString(), "1.222.33.4");
	EXPECT_EQ(retvalCmd.msg["port"].asString(), "1337");

	EXPECT_TRUE(cm.isConnected());
}

TEST(testConnect, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "connect";
	args = {"1.222.33.4", "1337"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	// cm.stateSetConnectionOk() not called

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::connect);
	EXPECT_EQ(retvalCmd.msg["address"].asString(), "1.222.33.4");
	EXPECT_EQ(retvalCmd.msg["port"].asString(), "1337");

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testConnect, AlreadyConnected) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initConnected();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "connect";
	args = {"1.222.33.4", "1337"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	// cm.stateSetConnectionOk() not called

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isConnected());
}

TEST(testConnect, AlreadyLoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "connect";
	args = {"1.222.33.4", "1337"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	// cm.stateSetConnectionOk() not called

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for
 * version check, signup, login
 */

TEST(testVersion, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initConnected();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "version";
	args = {};
	root["major"] = protocolMajorVersion;
	root["minor"] = protocolMinorVersion;
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["major"], protocolMajorVersion);
	EXPECT_EQ(retvalCmd.msg["minor"], protocolMinorVersion);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "version");
	EXPECT_EQ(retvalHdl.msg["serverversion"].asString(), std::to_string(protocolMajorVersion) + "." + std::to_string(protocolMinorVersion));
	EXPECT_EQ(retvalHdl.msg["clientversion"].asString(), std::to_string(protocolMajorVersion) + "." + std::to_string(protocolMinorVersion));
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isVersionChecked());
}

TEST(testVersion, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initConnected();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;
	int servermajor, serverminor;

	// prepare root/cmd/args
	cmd = "version";
	args = {};
	servermajor = (protocolMajorVersion == 0) ? 42 : 0;
	serverminor = (protocolMinorVersion == 3) ? 42 : 3;
	root["major"] = servermajor;
	root["minor"] = serverminor;
	root["accept"] = false;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["major"], protocolMajorVersion);
	EXPECT_EQ(retvalCmd.msg["minor"], protocolMinorVersion);

	EXPECT_EQ(retvalHdl.type, CmdMan::error | CmdMan::close);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "version");
	EXPECT_EQ(retvalHdl.msg["serverversion"].asString(), std::to_string(servermajor) + "." + std::to_string(serverminor));
	EXPECT_EQ(retvalHdl.msg["clientversion"].asString(), std::to_string(protocolMajorVersion) + "." + std::to_string(protocolMinorVersion));
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testVersion, AlreadyLoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args
	cmd = "version";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testLogin, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "login";
	args = {"usernem", "paswod"};
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_TRUE(retvalCmd.msg["login"].asBool());
	EXPECT_EQ(retvalCmd.msg["user"].asString(), "usernem");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "paswod");
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "login");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testLogin, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "login";
	args = {"usernem", "paswod"};
	root["accept"] = false;
	root["error"] = "fancy error string";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_TRUE(retvalCmd.msg["login"].asBool());
	EXPECT_EQ(retvalCmd.msg["user"].asString(), "usernem");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "paswod");
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_EQ(retvalHdl.type, CmdMan::error | CmdMan::close);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "login");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("fancy error string"));

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testLogin, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "login";
	args = {"usernem"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isVersionChecked());
}

TEST(testLogin, AlreadyLoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare root/cmd/args
	cmd = "login";
	args = {"usernem", "paswod"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "login");
	EXPECT_FALSE(retvalCmd.msg["accept"].asBool());
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testLogin, TwoRequestsInRow) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd1;
	CmdMan::CmdRet retvalCmd2;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "login";
	args = {"usernem", "paswod"};
	root["accept"] = true;

	// stick into cmdman
	retvalCmd1 = cm.execute(cmd, args);
	retvalCmd2 = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd1.type, CmdMan::send);
	EXPECT_TRUE(retvalCmd1.msg["login"].asBool());
	EXPECT_EQ(retvalCmd1.msg["user"].asString(), "usernem");
	EXPECT_EQ(retvalCmd1.msg["pass"].asString(), "paswod");
	EXPECT_FALSE(retvalCmd1.msg["cancel"].asBool());

	EXPECT_EQ(retvalCmd2.type, CmdMan::error);
	EXPECT_EQ(retvalCmd2.msg["command"].asString(), "login");
	EXPECT_FALSE(retvalCmd2.msg["accept"].asBool());
	EXPECT_NE(retvalCmd2.msg["error"].asString(), "");

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "login");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testSignup, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "signup";
	args = {"usernem", "paswod"};
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_FALSE(retvalCmd.msg["login"].asBool());
	EXPECT_EQ(retvalCmd.msg["user"].asString(), "usernem");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "paswod");
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "signup");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testSignup, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "signup";
	args = {"usernem", "paswod"};
	root["accept"] = false;
	root["error"] = "fancy error string";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_FALSE(retvalCmd.msg["login"].asBool());
	EXPECT_EQ(retvalCmd.msg["user"].asString(), "usernem");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "paswod");
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "signup");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("fancy error string"));

	EXPECT_TRUE(cm.isVersionChecked());
}

TEST(testSignup, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "signup";
	args = {"usernem"};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isVersionChecked());
}

/*
 * =====================================
 * tests for disconnect and exit
 */
TEST(testDisconnect, LoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "disconnect";
	args = {};
	root["command"] = "close";
	root["response"] = "bye";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "close");

	EXPECT_EQ(retvalHdl.type, CmdMan::close | CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "disconnect");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testDisconnect, NotLoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "disconnect";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send | CmdMan::close | CmdMan::noanswerexpected);
	EXPECT_FALSE(retvalCmd.msg["login"].asBool());
	EXPECT_EQ(retvalCmd.msg["user"].asString(), "");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "");
	EXPECT_TRUE(retvalCmd.msg["cancel"].asBool());

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testExit, LoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmdExit;
	CmdMan::CmdRet retvalCmdDisconnect;
	CmdMan::CmdRet retvalHdl;
	CmdMan::CmdRet retvalLastExit;

	// prepare root/cmd/args
	cmd = "exit";
	args = {};
	root["command"] = "close";
	root["response"] = "bye";

	// stick into cmdman and check things
	retvalCmdExit = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmdExit.type, CmdMan::none);
	EXPECT_EQ(retvalCmdExit.nextcommand, "disconnect");

	retvalCmdDisconnect = cm.execute("disconnect", args);

	EXPECT_EQ(retvalCmdDisconnect.type, CmdMan::send);
	EXPECT_EQ(retvalCmdDisconnect.msg["command"].asString(), "close");

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::close | CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "disconnect");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.nextcommand, "exit");

	EXPECT_TRUE(cm.isNotConnected());

	retvalLastExit = cm.execute("exit", args);

	EXPECT_EQ(retvalLastExit.type, CmdMan::exit);
	EXPECT_EQ(retvalLastExit.msg["command"].asString(), "exit");
}

TEST(testExit, NotLoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initVersionChecked();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmdExit;
	CmdMan::CmdRet retvalCmdDisconnect;
	CmdMan::CmdRet retvalHdl;
	CmdMan::CmdRet retvalLastExit;

	// prepare root/cmd/args
	cmd = "exit";
	args = {};
	root["command"] = "close";
	root["response"] = "bye";

	// stick into cmdman and check things
	retvalCmdExit = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmdExit.type, CmdMan::none);
	EXPECT_EQ(retvalCmdExit.nextcommand, "disconnect");

	retvalCmdDisconnect = cm.execute("disconnect", args);

	EXPECT_EQ(retvalCmdDisconnect.type, CmdMan::send | CmdMan::close | CmdMan::noanswerexpected);
	EXPECT_FALSE(retvalCmdDisconnect.msg["login"].asBool());
	EXPECT_EQ(retvalCmdDisconnect.msg["user"].asString(), "");
	EXPECT_EQ(retvalCmdDisconnect.msg["pass"].asString(), "");
	EXPECT_TRUE(retvalCmdDisconnect.msg["cancel"].asBool());
	EXPECT_EQ(retvalCmdDisconnect.nextcommand, "exit");

	EXPECT_TRUE(cm.isNotConnected());

	retvalLastExit = cm.execute("exit", args);

	EXPECT_EQ(retvalLastExit.type, CmdMan::exit);
	EXPECT_EQ(retvalLastExit.msg["command"].asString(), "exit");
}

TEST(testExit, NotConnected) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare root/cmd/args
	cmd = "exit";
	args = {};

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::exit);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "exit");
}

/* =====================================
 * tests for put[data]
 */

TEST(testPut, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "put";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPut, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "put";
	args = {"../some/nice/filepath/filename.txt"};
	root["command"] = "put";
	root["file"] = "filename.txt";
	root["accept"] = true;

	EXPECT_CALL(fm, openPut(testing::_)).WillOnce(testing::Return(true));
	ON_CALL(fm, getPutSize()).WillByDefault(testing::Return(5));
	ON_CALL(fm, getPutChunks()).WillByDefault(testing::Return(1));
	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));

	EXPECT_CALL(fm, cancelGet).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "put");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalCmd.msg["size"].asInt(), 5);
	EXPECT_EQ(retvalCmd.msg["chunks"].asInt(), 1);

	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print | CmdMan::send);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "put");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.nextcommand, "putdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Positive_LastChunk) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	/**logically continuation of TEST(testPut, Positive)**/

	// prepare root/cmd/args
	cmd = "putdata";
	args = {};
	root["command"] = "putdata";
	root["file"] = "filename.txt";
	root["received"] = 0;
	root["cancel"] = false;

	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, readBase64()).WillByDefault(testing::Return("cool Base64 string"));
	ON_CALL(fm, getPutRemainingChunks()).WillByDefault(testing::Return(0));
	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelPut).Times(0); // not called
	EXPECT_CALL(fm, closePut);

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "putdata");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalCmd.msg["data"].asString(), "cool Base64 string");
	EXPECT_EQ(retvalCmd.msg["remaining"].asInt(), 0);
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "putdata");
	EXPECT_FALSE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["speed"].asInt(), 0);
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Positive_ChunksRemaining) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "putdata";
	args = {};
	root["command"] = "putdata";
	root["file"] = "filename.txt";
	root["received"] = 42;
	root["cancel"] = false;

	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, readBase64()).WillByDefault(testing::Return("cool Base64 string"));
	ON_CALL(fm, getPutRemainingChunks()).WillByDefault(testing::Return(42));
	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelGet).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "putdata");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalCmd.msg["data"].asString(), "cool Base64 string");
	EXPECT_EQ(retvalCmd.msg["remaining"].asInt(), 42);
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print | CmdMan::send);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "putdata");
	EXPECT_FALSE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["speed"].asInt(), 0);
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.msg["error"].asString(), "");
	EXPECT_EQ(retvalHdl.nextcommand, "putdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPut, Negative_FileNotExisting) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "put";
	args = {"../some/nice/filepath/filename.txt"};

	EXPECT_CALL(fm, openPut(testing::_)).WillOnce(testing::Return(false));
	ON_CALL(fm, pathToFilename("../some/nice/filepath/filename.txt")).WillByDefault(testing::Return("filename.txt"));

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "put");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPut, Negative_ServerError) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "put";
	root["file"] = "filename.txt";
	root["accept"] = false;
	root["error"] = "foobar";

	ON_CALL(fm, getPutSize()).WillByDefault(testing::Return(5));
	ON_CALL(fm, getPutChunks()).WillByDefault(testing::Return(1));
	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));

	EXPECT_CALL(fm, cancelPut);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "put");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("foobar"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPut, Negative_ServerWrongFile) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "put";
	root["file"] = "completely_different_filename.txt";
	root["accept"] = true;

	ON_CALL(fm, getPutSize()).WillByDefault(testing::Return(5));
	ON_CALL(fm, getPutChunks()).WillByDefault(testing::Return(1));
	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));

	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));
	EXPECT_CALL(fm, cancelPut);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "put");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Negative_ServerError) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "putdata";
	root["file"] = "filename.txt";
	root["received"] = 42;
	root["cancel"] = true;
	root["error"] = "cool foobar";

	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, getPutRemainingChunks()).WillByDefault(testing::Return(42));
	ON_CALL(fm, isPutting()).WillByDefault(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_CALL(fm, cancelPut);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "putdata");
	EXPECT_TRUE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("cool foobar"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Negative_ServerWrongFile) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "putdata";
	root["file"] = "completely_different_filename.txt";
	root["received"] = 42;
	root["cancel"] = false;

	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, getPutRemainingChunks()).WillByDefault(testing::Return(42));
	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelPut);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "putdata");
	EXPECT_TRUE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Negative_ServerWrongChunkNumber) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "putdata";
	root["file"] = "filename.txt";
	root["received"] = 23;
	root["cancel"] = false;

	ON_CALL(fm, getPutName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, getPutRemainingChunks()).WillByDefault(testing::Return(42));
	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelPut);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "putdata");
	EXPECT_TRUE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPut, Negative_NoFileOpen_ServerMessage) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "put";
	root["file"] = "filename.txt";
	root["accept"] = true;

	// handle reply
	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(false));
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "put");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Negative_NoFileOpen_ServerMessage) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "putdata";
	root["file"] = "filename.txt";
	root["received"] = 23;
	root["cancel"] = false;

	// handle reply
	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(false));
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "putdata");
	//	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt"); // no guarantee should be given...
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testPutdata, Negative_NoFileOpen_UserRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	cmd = "putdata";
	args = {};

	EXPECT_CALL(fm, isPutting()).WillOnce(testing::Return(false));

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "putdata");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for head
 */
TEST(testHead, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args
	cmd = "head";
	args = {};

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	retvalHdl = cm.handle(root);

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testHead, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "head";
	args = {"filename.txt"};
	root["command"] = "head";
	root["file"] = "filename.txt";
	root["accept"] = true;
	root["data"] = "this string is 4 bytes (obviously)";

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "head");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "head");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.msg["data"].asString(), "this string is 4 bytes (obviously)");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testHead, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "head";
	args = {"filename.txt"};
	root["command"] = "head";
	root["file"] = "filename.txt";
	root["accept"] = false;
	root["error"] = "this is a fancy error message";

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "head");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "head");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("this is a fancy error message"));

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for get[data]
 */

TEST(testGet, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "get";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGet, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "get";
	args = {"../some/nice/filepath/filename.txt"};
	root["command"] = "get";
	root["file"] = "filename.txt";
	root["accept"] = true;
	root["chunks"] = 42;

	EXPECT_CALL(fm, openGet("../some/nice/filepath/filename.txt")).WillOnce(testing::Return(true));
	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));

	EXPECT_CALL(fm, cancelGet).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "get");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");

	EXPECT_CALL(fm, isGetting()).WillOnce(testing::Return(true));
	EXPECT_CALL(fm, setGetChunks(42));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print | CmdMan::send);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "get");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.nextcommand, "getdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGetdata, Positive_LastChunk) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	/**logically continuation of TEST(testGet, Positive)**/

	// prepare root/cmd/args
	cmd = "getdata";
	args = {};
	root["command"] = "getdata";
	root["file"] = "filename.txt";
	root["remaining"] = 0;
	root["cancel"] = false;
	root["data"] = "cool Base64 string";

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));
	EXPECT_CALL(fm, writeBase64("cool Base64 string"));
	ON_CALL(fm, getGetRemainingChunks()).WillByDefault(testing::Return(0));
	EXPECT_CALL(fm, isGetting()).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelGet).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "getdata");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalCmd.msg["chunk"].asInt(), 0);
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_CALL(fm, isGetting()).WillOnce(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "getdata");
	EXPECT_FALSE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["speed"].asInt(), 0);
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGetdata, Positive_ChunksRemaining) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "getdata";
	args = {};
	root["command"] = "getdata";
	root["file"] = "filename.txt";
	root["remaining"] = 42;
	root["cancel"] = false;
	root["data"] = "cool Base64 string";

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));
	EXPECT_CALL(fm, writeBase64("cool Base64 string"));
	ON_CALL(fm, getGetRemainingChunks()).WillByDefault(testing::Return(42));
	EXPECT_CALL(fm, isGetting()).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelGet).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "getdata");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalCmd.msg["chunk"].asInt(), 42);
	EXPECT_FALSE(retvalCmd.msg["cancel"].asBool());

	EXPECT_CALL(fm, isGetting()).WillOnce(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print | CmdMan::send);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "getdata");
	EXPECT_FALSE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["speed"].asInt(), 0);
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_EQ(retvalHdl.msg["error"].asString(), "");
	EXPECT_EQ(retvalHdl.nextcommand, "getdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGet, Negative_FileAlreadyExisting) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "get";
	args = {"../some/nice/filepath/filename.txt"};

	EXPECT_CALL(fm, openGet(testing::_)).WillOnce(testing::Return(false));
	ON_CALL(fm, pathToFilename("../some/nice/filepath/filename.txt")).WillByDefault(testing::Return("filename.txt"));

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "get");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGet, Negative_ServerError) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "get";
	root["file"] = "filename.txt";
	root["accept"] = false;
	root["error"] = "foobar";

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));
	EXPECT_CALL(fm, cancelGet());

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "get");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGet, Negative_ServerWrongFile) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "get";
	root["file"] = "completely_different_filename.txt";
	root["accept"] = true;

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));

	EXPECT_CALL(fm, isGetting()).WillOnce(testing::Return(true));
	EXPECT_CALL(fm, cancelGet);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "get");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGetdata, Negative_ServerError) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "getdata";
	root["file"] = "filename.txt";
	root["remaining"] = 42;
	root["cancel"] = true;
	root["error"] = "fancy foobar";

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, getGetRemainingChunks()).WillByDefault(testing::Return(42));
	ON_CALL(fm, isGetting()).WillByDefault(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_CALL(fm, cancelGet);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "getdata");
	EXPECT_TRUE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("fancy foobar"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGetdata, Negative_ServerWrongFile) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "getdata";
	root["file"] = "completely_different_filename.txt";
	root["remaining"] = 42;
	root["cancel"] = false;

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, getGetRemainingChunks()).WillByDefault(testing::Return(42));
	ON_CALL(fm, isGetting()).WillByDefault(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_CALL(fm, cancelGet);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "getdata");
	EXPECT_TRUE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testGetdata, Negative_ServerWrongChunkNumber) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "getdata";
	root["file"] = "filename.txt";
	root["remaining"] = 23;
	root["cancel"] = false;

	ON_CALL(fm, getGetName()).WillByDefault(testing::Return("filename.txt"));
	ON_CALL(fm, getGetRemainingChunks()).WillByDefault(testing::Return(42));
	ON_CALL(fm, isGetting()).WillByDefault(testing::Return(true));
	retvalHdl = cm.handle(root);

	EXPECT_CALL(fm, cancelGet);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "getdata");
	EXPECT_TRUE(retvalHdl.msg["cancel"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "filename.txt");
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for list[data]
 */

TEST(testList, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "list";
	args = {};
	root["command"] = "list";
	root["accept"] = true;
	root["items"] = 92;
	root["chunks"] = 2;

	EXPECT_CALL(fm, openList(false)).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "list");

	EXPECT_CALL(fm, isListingSimple()).WillOnce(testing::Return(true));
	EXPECT_CALL(fm, setListChunks(2));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::send);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "list");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.nextcommand, "listdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testListdata, Positive_ChunksRemaining) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, namesArr;
	vector<string> namevec = {"blue.txt", "red.pdf", "green.sh", "yellow.tgz"};
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "listdata";
	args = {};
	root["command"] = "listdata";
	root["cancel"] = false;
	root["remaining"] = 3;
	for (string s : namevec)
		namesArr.append(s);
	root["names"] = namesArr;

	EXPECT_CALL(fm, getListRemainingChunks).Times(2).WillRepeatedly(testing::Return(3));
	EXPECT_CALL(fm, isListingSimple).Times(2).WillRepeatedly(testing::Return(true));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "listdata");
	EXPECT_EQ(retvalCmd.msg["chunk"].asInt(), 3);
	EXPECT_EQ(retvalCmd.msg["cancel"].asBool(), false);

	vector<Json::Value> vec{Json::Value("blue.txt"), Json::Value("red.pdf"), Json::Value("green.sh"), Json::Value("yellow.tgz")};
	EXPECT_CALL(fm, putListData(vec));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::send);
	EXPECT_EQ(retvalHdl.nextcommand, "listdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testListdata, Positive_LastChunk) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, namesArr;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "listdata";
	args = {};
	root["command"] = "listdata";
	root["cancel"] = false;
	root["remaining"] = 0;
	namesArr.append("magenta.vcd");
	root["names"] = namesArr;

	EXPECT_CALL(fm, getListRemainingChunks).Times(2).WillRepeatedly(testing::Return(0));
	EXPECT_CALL(fm, isListingSimple).Times(2).WillRepeatedly(testing::Return(true));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "listdata");
	EXPECT_EQ(retvalCmd.msg["chunk"].asInt(), 0);
	EXPECT_EQ(retvalCmd.msg["cancel"].asBool(), false);

	vector<Json::Value> vec{Json::Value("magenta.vcd")};
	EXPECT_CALL(fm, putListData(vec));
	EXPECT_CALL(fm, closeList);
	vector<Json::Value> allFiles{"blue.txt", "red.pdf", "green.sh", "yellow.tgz", "cyan.exe", "white", "black", "magenta.vcd"};
	EXPECT_CALL(fm, getListData).WillOnce(testing::Return(allFiles));

	retvalHdl = cm.handle(root);
	// convert returned json value into vector
	vector<string> retval_msg_names;
	for (Json::Value i : retvalHdl.msg["names"])
		retval_msg_names.push_back(i.asString());

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "list");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	vector<string> allFilesStringVec{"blue.txt", "red.pdf", "green.sh", "yellow.tgz", "cyan.exe", "white", "black", "magenta.vcd"};
	EXPECT_EQ(retval_msg_names, allFilesStringVec);

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testList, Negative_ListEmpty) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "list";
	args = {};
	root["command"] = "list";
	root["accept"] = true;
	root["items"] = 0;

	// stick into cmdman and check things
	EXPECT_CALL(fm, openList(false)).WillOnce(testing::Return(true));
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "list");

	EXPECT_CALL(fm, closeList);
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "list");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("There are no files stored on the server."));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testListdata, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	root["command"] = "listdata";
	root["cancel"] = true;
	root["error"] = "some foobar";

	EXPECT_CALL(fm, cancelList);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "list");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("some foobar"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testList, Negative_AlreadyListing) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "list";
	args = {};

	EXPECT_CALL(fm, openList(false)).WillOnce(testing::Return(false));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "list");
	EXPECT_FALSE(retvalCmd.msg["accept"].asBool());
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testList, Negative_NotListing_ServerRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	Json::Value root, namesArr;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	root["command"] = "listdata";
	root["cancel"] = false;
	root["remaining"] = 0;
	namesArr.append("magenta.vcd");
	root["names"] = namesArr;

	EXPECT_CALL(fm, cancelList).Times(0);
	EXPECT_CALL(fm, putListData).Times(0);
	EXPECT_CALL(fm, closeList).Times(0);
	EXPECT_CALL(fm, getListData).Times(0);
	EXPECT_CALL(fm, isListingSimple).WillOnce(testing::Return(false));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "list");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testListdata, Negative_NotListing_UserRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare root/cmd/args
	cmd = "listdata";
	args = {};

	EXPECT_CALL(fm, isListingSimple).WillOnce(testing::Return(false));

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "list");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");
	EXPECT_EQ(retvalCmd.msg["accept"].asBool(), false);
}

TEST(testListdata, Negative_NotListing_ServerRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, namesArr;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	root["command"] = "listdata";
	root["cancel"] = false;
	root["remaining"] = 0;
	namesArr.append("magenta.vcd");
	root["names"] = namesArr;

	EXPECT_CALL(fm, cancelList).Times(0);
	EXPECT_CALL(fm, isListingSimple).WillOnce(testing::Return(false));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "list");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for extendedlist[data]
 */

TEST(testExtendedlist, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "extendedlist";
	args = {};
	root["command"] = "extendedlist";
	root["accept"] = true;
	root["items"] = 92;
	root["chunks"] = 2;

	EXPECT_CALL(fm, openList(true)).WillOnce(testing::Return(true));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedlist");

	EXPECT_CALL(fm, isListingExtended()).WillOnce(testing::Return(true));
	EXPECT_CALL(fm, setListChunks(2));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::send);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedlist");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.nextcommand, "extendedlistdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedlistdata, Positive_ChunksRemaining) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, files, f1, f2, namesArr;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	f1["name"] = "blue.txt";
	f1["size"] = 42.0f;
	f1["head"] = "some head 1";
	files.append(f1);
	f2["name"] = "green.pdf";
	f2["size"] = 9.0f;
	f2["head"] = "some head 2";
	files.append(f2);

	cmd = "extendedlistdata";
	args = {};
	root["command"] = "extendedlistdata";
	root["cancel"] = false;
	root["remaining"] = 3;
	root["files"] = files;

	EXPECT_CALL(fm, getListRemainingChunks).Times(2).WillRepeatedly(testing::Return(3));
	EXPECT_CALL(fm, isListingExtended).Times(2).WillRepeatedly(testing::Return(true));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedlistdata");
	EXPECT_EQ(retvalCmd.msg["chunk"].asInt(), 3);
	EXPECT_EQ(retvalCmd.msg["cancel"].asBool(), false);

	vector<Json::Value> vec{f1, f2};
	EXPECT_CALL(fm, putListData(vec));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::send);
	EXPECT_EQ(retvalHdl.nextcommand, "extendedlistdata");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedlistdata, Positive_LastChunk) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, files, f1, f2, f3, inputfile;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare file json/root/cmd/args

	inputfile["name"] = "magenta.vcd";
	inputfile["size"] = 9000.001f;
	inputfile["head"] = "some head 3";
	files.append(inputfile);

	cmd = "extendedlistdata";
	args = {};
	root["command"] = "extendedlistdata";
	root["cancel"] = false;
	root["remaining"] = 0;
	root["files"] = files;

	EXPECT_CALL(fm, getListRemainingChunks).Times(2).WillRepeatedly(testing::Return(0));
	EXPECT_CALL(fm, isListingExtended).Times(2).WillRepeatedly(testing::Return(true));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedlistdata");
	EXPECT_EQ(retvalCmd.msg["chunk"].asInt(), 0);
	EXPECT_EQ(retvalCmd.msg["cancel"].asBool(), false);

	vector<Json::Value> vec{inputfile};
	EXPECT_CALL(fm, putListData(vec));
	EXPECT_CALL(fm, closeList);
	// output of fileman
	f1["name"] = "blue.txt";
	f1["size"] = 42.0f;
	f1["encrypted"] = "unencrypted";
	f2["name"] = "green.pdf";
	f2["size"] = 9.0f;
	f2["encrypted"] = "decryptable";
	f3["name"] = "magenta.vcd";
	f3["size"] = 9000.001f;
	f3["encrypted"] = "undecryptable";
	vector<Json::Value> allFiles{f1, f2, f3};
	EXPECT_CALL(fm, getListData).WillOnce(testing::Return(allFiles));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedlist");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["files"][0]["name"].asString(), "blue.txt");
	EXPECT_EQ(retvalHdl.msg["files"][0]["encrypted"].asString(), "unencrypted");
	EXPECT_EQ(retvalHdl.msg["files"][0]["size"].asFloat(), 42.0f);
	EXPECT_EQ(retvalHdl.msg["files"][1]["name"].asString(), "green.pdf");
	EXPECT_EQ(retvalHdl.msg["files"][1]["encrypted"].asString(), "decryptable");
	EXPECT_EQ(retvalHdl.msg["files"][1]["size"].asFloat(), 9.0f);
	EXPECT_EQ(retvalHdl.msg["files"][2]["name"].asString(), "magenta.vcd");
	EXPECT_EQ(retvalHdl.msg["files"][2]["encrypted"].asString(), "undecryptable");
	EXPECT_EQ(retvalHdl.msg["files"][2]["size"].asFloat(), 9000.001f);

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedlist, Negative_ListEmpty) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	cmd = "extendedlist";
	args = {};
	root["command"] = "extendedlist";
	root["accept"] = true;
	root["items"] = 0;

	// stick into cmdman and check things
	EXPECT_CALL(fm, openList(true)).WillOnce(testing::Return(true));
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedlist");

	EXPECT_CALL(fm, closeList);
	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedlist");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("There are no files stored on the server."));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedlistdata, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	/* tests the handling of server reply only,
	 * initial request assumed as correct (tested in other tests) */

	Json::Value root;
	CmdMan::CmdRet retvalHdl;

	// prepare root/cmd/args
	root["command"] = "extendedlistdata";
	root["cancel"] = true;
	root["error"] = "some foobar";

	EXPECT_CALL(fm, cancelList);

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedlist");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("some foobar"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedlist, Negative_AlreadyListing) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "extendedlist";
	args = {};

	EXPECT_CALL(fm, openList(true)).WillOnce(testing::Return(false));

	EXPECT_CALL(fm, cancelList).Times(0); // not called

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedlist");
	EXPECT_FALSE(retvalCmd.msg["accept"].asBool());
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedlist, Negative_NotListing_ServerRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, files, inputfile;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	inputfile["name"] = "magenta.vcd";
	inputfile["size"] = 9000.001f;
	inputfile["head"] = "some head 3";
	files.append(inputfile);

	root["command"] = "extendedlistdata";
	root["cancel"] = false;
	root["remaining"] = 0;
	root["files"] = files;

	EXPECT_CALL(fm, cancelList).Times(0);
	EXPECT_CALL(fm, putListData).Times(0);
	EXPECT_CALL(fm, closeList).Times(0);
	EXPECT_CALL(fm, getListData).Times(0);
	EXPECT_CALL(fm, isListingExtended).WillOnce(testing::Return(false));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedlist");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedistdata, Negative_NotListing_UserRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare root/cmd/args
	cmd = "extendedlistdata";
	args = {};

	EXPECT_CALL(fm, isListingExtended).WillOnce(testing::Return(false));

	// stick into cmdman and check things
	retvalCmd = cm.execute(cmd, args);

	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedlist");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");
	EXPECT_EQ(retvalCmd.msg["accept"].asBool(), false);
}

TEST(testExtendedlistdata, Negative_NotListing_ServerRequest) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, files, inputfile;
	CmdMan::CmdRet retvalHdl;

	// prepare root
	inputfile["name"] = "magenta.vcd";
	inputfile["size"] = 9000.001f;
	inputfile["head"] = "some head 3";
	files.append(inputfile);

	root["command"] = "extendedlistdata";
	root["cancel"] = false;
	root["remaining"] = 0;
	root["files"] = files;

	EXPECT_CALL(fm, cancelList).Times(0);
	EXPECT_CALL(fm, isListingExtended).WillOnce(testing::Return(false));

	retvalHdl = cm.handle(root);

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedlist");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_NE(retvalHdl.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for deleteme
 */

TEST(testDeleteme, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "deleteme";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testDeleteme, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "deleteme";
	args = {"myFancyPasswd"};
	root["command"] = "deleteme";
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "deleteme");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "myFancyPasswd");
	EXPECT_EQ(retvalHdl.type, CmdMan::print | CmdMan::close);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "deleteme");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testDeleteme, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "deleteme";
	args = {"myFancyPasswd"};
	root["command"] = "deleteme";
	root["accept"] = false;
	root["error"] = "password incorrect";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "deleteme");
	EXPECT_EQ(retvalCmd.msg["pass"].asString(), "myFancyPasswd");
	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "deleteme");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("password incorrect"));

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for deletefile
 */
TEST(testDeletefile, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "deletefile";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testDeletefile, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "deletefile";
	args = {"fancy_file.txt"};
	root["command"] = "deletefile";
	root["file"] = "fancy_file.txt";
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "deletefile");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "fancy_file.txt");
	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "deletefile");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "fancy_file.txt");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testDeletefile, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "deletefile";
	args = {"fancy_file.txt"};
	root["command"] = "deletefile";
	root["file"] = "fancy_file.txt";
	root["accept"] = false;
	root["error"] = "file does not exist";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "deletefile");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "fancy_file.txt");
	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "deletefile");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "fancy_file.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("file does not exist"));

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * test for status
 */
TEST(testStatus, LoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// set ip, port and user name fields of the cmdman
	cm.execute("connect", {"1.2.3.4", "1337"});
	cm.initVersionChecked();
	cm.execute("login", {"usernem", "paswod"});
	cm.initLoggedIn();

	// prepare cmd/args/root
	cmd = "status";
	args = {};
	root["command"] = "status";
	root["response"] = "fancy response";
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "status");
	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "status");
	EXPECT_THAT(retvalHdl.msg["response"].asString(), testing::HasSubstr("fancy response"));
	EXPECT_THAT(retvalHdl.msg["response"].asString(), testing::HasSubstr("1.2.3.4"));
	EXPECT_THAT(retvalHdl.msg["response"].asString(), testing::HasSubstr("1337"));
	EXPECT_THAT(retvalHdl.msg["response"].asString(), testing::HasSubstr("usernem"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testStatus, NotConnected) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// set ip and port fields of the cmdman
	cm.execute("connect", {"1.2.3.4"});
	cm.initNotConnected();

	// prepare cmd/args
	cmd = "status";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::print);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "status");
	EXPECT_NE(retvalCmd.msg["response"].asString(), "");
	EXPECT_THAT(retvalCmd.msg["response"].asString(), testing::Not(testing::HasSubstr("1.2.3.4")));

	EXPECT_TRUE(cm.isNotConnected());
}

TEST(testStatus, NotLoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// set ip, port and user name fields of the cmdman
	cm.initConnected();
	cm.execute("login", {"usernem", "paswod"});
	cm.initNotConnected();
	cm.execute("connect", {"1.2.3.4", "1337"});
	cm.initVersionChecked(); // not logged in

	// prepare cmd/args
	cmd = "status";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::print);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "status");
	EXPECT_THAT(retvalCmd.msg["response"].asString(), testing::HasSubstr("1.2.3.4"));
	EXPECT_THAT(retvalCmd.msg["response"].asString(), testing::HasSubstr("1337"));
	EXPECT_THAT(retvalCmd.msg["response"].asString(), testing::Not(testing::HasSubstr("usernem")));

	EXPECT_TRUE(cm.isVersionChecked());
}

/* =====================================
 * test for extendedstatus
 */

TEST(testExtendedstatus, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "extendedstatus";
	args = {};
	root["command"] = "extendedstatus";
	root["accept"] = false;
	root["error"] = "some foobar went wrong";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedstatus");
	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedstatus");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("some foobar went wrong"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedstatus, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, transfersclientserver, tcs1, tcs2, transfersserverserver, tss1, tss2, tss3;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "extendedstatus";
	args = {};
	root["command"] = "extendedstatus";
	root["accept"] = true;

	tcs1["upload"] = true;
	tcs1["file"] = "green.txt";
	tcs1["progress"] = 99;
	transfersclientserver.append(tcs1);
	tcs2["upload"] = false;
	tcs2["file"] = "red.txt";
	tcs2["progress"] = 2;
	transfersclientserver.append(tcs2);
	root["transfersclientserver"] = transfersclientserver;

	tss1["type"] = "upload";
	tss1["file"] = "cyan.txt";
	tss1["progress"] = 100;
	tss1["method"] = "urg field";
	tss1["speed"] = 42.0f;
	transfersserverserver.append(tss1);
	tss2["type"] = "download";
	tss2["file"] = "magenta.txt";
	tss2["progress"] = 42;
	tss2["method"] = "fancy thing";
	tss2["speed"] = 0.1f;
	transfersserverserver.append(tss2);
	tss3["type"] = "queued";
	tss3["file"] = "yellow.txt";
	tss3["progress"] = 42;
	tss3["method"] = "cool timing stuff";
	tss3["speed"] = 9000.001f;
	transfersserverserver.append(tss3);
	root["transfersserverserver"] = transfersserverserver;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedstatus");
	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedstatus");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	EXPECT_TRUE(retvalHdl.msg["transfersclientserver"][0]["upload"].asBool());
	EXPECT_EQ(retvalHdl.msg["transfersclientserver"][0]["file"].asString(), "green.txt");
	EXPECT_EQ(retvalHdl.msg["transfersclientserver"][0]["progress"].asInt(), 99);
	EXPECT_FALSE(retvalHdl.msg["transfersclientserver"][1]["upload"].asBool());
	EXPECT_EQ(retvalHdl.msg["transfersclientserver"][1]["file"].asString(), "red.txt");
	EXPECT_EQ(retvalHdl.msg["transfersclientserver"][1]["progress"].asInt(), 2);

	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][0]["type"].asString(), "upload");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][0]["file"].asString(), "cyan.txt");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][0]["progress"].asInt(), 100);
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][0]["method"].asString(), "urg field");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][0]["speed"].asFloat(), 42.0f);
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][1]["type"].asString(), "download");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][1]["file"].asString(), "magenta.txt");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][1]["progress"].asInt(), 42);
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][1]["method"].asString(), "fancy thing");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][1]["speed"].asFloat(), 0.1f);
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][2]["type"].asString(), "queued");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][2]["file"].asString(), "yellow.txt");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][2]["progress"].asInt(), 42);
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][2]["method"].asString(), "cool timing stuff");
	EXPECT_EQ(retvalHdl.msg["transfersserverserver"][2]["speed"].asFloat(), 9000.001f);

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testExtendedstatus, Positive_NoTransfersRunning) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, transfersclientserver, transfersserverserver;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "extendedstatus";
	args = {};
	root["command"] = "extendedstatus";
	root["accept"] = true;
	root["transfersclientserver"] = transfersclientserver;
	root["transfersserverserver"] = transfersserverserver;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "extendedstatus");
	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "extendedstatus");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_TRUE(retvalHdl.msg["transfersclientserver"].empty());
	EXPECT_TRUE(retvalHdl.msg["transfersserverserver"].empty());

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for help
 */
TEST(testHelp, LoggedIn) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args/root
	cmd = "help";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::print);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "help");
	EXPECT_NE(retvalCmd.msg["names"], "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testHelp, NotConnected) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args/root
	cmd = "help";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::print);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "help");
	EXPECT_NE(retvalCmd.msg["names"], "");

	EXPECT_TRUE(cm.isNotConnected());
}

/* =====================================
 * tests for keyfile and closekey
 */
TEST(testKeyfile, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "keyfile";
	args = {};

	// stick into cmdman
	EXPECT_CALL(fm, openKey).Times(0);
	EXPECT_CALL(fm, getOpensslError).Times(0);
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testKeyfile, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "keyfile";
	args = {"./some/path"};

	// stick into cmdman
	EXPECT_CALL(fm, openKey).WillOnce(testing::Return(false));
	EXPECT_CALL(fm, getOpensslError).WillOnce(testing::Return("some openssl error"));
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "keyfile");
	EXPECT_FALSE(retvalCmd.msg["accept"].asBool());
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "./some/path");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testKeyfile, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "keyfile";
	args = {"./some/path"};

	// stick into cmdman
	EXPECT_CALL(fm, openKey).WillOnce(testing::Return(true));
	EXPECT_CALL(fm, getOpensslError).Times(0);
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::print);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "keyfile");
	EXPECT_TRUE(retvalCmd.msg["accept"].asBool());
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "./some/path");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testClosekey, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "closekey";
	args = {};

	// stick into cmdman
	EXPECT_CALL(fm, closeKey).WillOnce(testing::Return(true));
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::print);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "closekey");
	EXPECT_TRUE(retvalCmd.msg["accept"].asBool());

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testClosekey, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "closekey";
	args = {};

	// stick into cmdman
	EXPECT_CALL(fm, closeKey).WillOnce(testing::Return(false));
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "closekey");
	EXPECT_FALSE(retvalCmd.msg["accept"].asBool());
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * test for notifications
 */
TEST(testNotifications, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, messagesArr;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;
	vector<string> msgvec = {"msg1", "msg2", "msg3 foo", "msg4 hello world!"};

	// prepare cmd/args/root
	cmd = "notifications";
	args = {};
	root["command"] = "notifications";
	for (string s : msgvec)
		messagesArr.append(s);
	root["messages"] = messagesArr;
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "notifications");

	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "notifications");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());

	vector<string> retval_msgs;
	for (Json::Value i : retvalHdl.msg["messages"])
		retval_msgs.push_back(i.asString());
	EXPECT_EQ(retval_msgs, msgvec);

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testNotifications, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root, messagesArr;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "notifications";
	args = {};
	root["command"] = "notifications";
	root["accept"] = false;
	root["error"] = "some very descriptive string";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "notifications");

	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "notifications");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("some very descriptive string"));

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * tests for queue and dequeue
 */
TEST(testQueue, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "queue";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testQueue, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "queue";
	args = {"fancy_file.txt"};
	root["command"] = "queue";
	root["file"] = "fancy_file.txt";
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "queue");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "fancy_file.txt");
	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "queue");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "fancy_file.txt");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testQueue, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "queue";
	args = {"fancy_file.txt"};
	root["command"] = "queue";
	root["file"] = "fancy_file.txt";
	root["accept"] = false;
	root["error"] = "file does not exist";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "queue");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "fancy_file.txt");
	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "queue");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "fancy_file.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("file does not exist"));

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testDequeue, TooFewArgs) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	CmdMan::CmdRet retvalCmd;

	// prepare cmd/args
	cmd = "dequeue";
	args = {};

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::error);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "error");
	EXPECT_NE(retvalCmd.msg["error"].asString(), "");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testDequeue, Positive) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "dequeue";
	args = {"fancy_file.txt"};
	root["command"] = "dequeue";
	root["file"] = "fancy_file.txt";
	root["accept"] = true;

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "dequeue");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "fancy_file.txt");
	EXPECT_EQ(retvalHdl.type, CmdMan::print);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "dequeue");
	EXPECT_TRUE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "fancy_file.txt");

	EXPECT_TRUE(cm.isLoggedIn());
}

TEST(testDequeue, Negative) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	std::string cmd;
	std::vector<std::string> args;
	Json::Value root;
	CmdMan::CmdRet retvalCmd;
	CmdMan::CmdRet retvalHdl;

	// prepare cmd/args/root
	cmd = "dequeue";
	args = {"fancy_file.txt"};
	root["command"] = "dequeue";
	root["file"] = "fancy_file.txt";
	root["accept"] = false;
	root["error"] = "file not in queue";

	// stick into cmdman
	retvalCmd = cm.execute(cmd, args);
	retvalHdl = cm.handle(root);

	// check things
	EXPECT_EQ(retvalCmd.type, CmdMan::send);
	EXPECT_EQ(retvalCmd.msg["command"].asString(), "dequeue");
	EXPECT_EQ(retvalCmd.msg["file"].asString(), "fancy_file.txt");
	EXPECT_EQ(retvalHdl.type, CmdMan::error);
	EXPECT_EQ(retvalHdl.msg["command"].asString(), "dequeue");
	EXPECT_FALSE(retvalHdl.msg["accept"].asBool());
	EXPECT_EQ(retvalHdl.msg["file"].asString(), "fancy_file.txt");
	EXPECT_THAT(retvalHdl.msg["error"].asString(), testing::HasSubstr("file not in queue"));

	EXPECT_TRUE(cm.isLoggedIn());
}

/* =====================================
 * test for all commands that require a login
 */
TEST(testCommandsWithRequiredLogin, NotLoggedInOrNotConnected) {
	FileManMock fm;
	CmdManForTest cm(fm, dummyDebugPrint);
	cm.initLoggedIn();

	CmdMan::CmdRet retval1, retval2, retval3;

	// prepare cmd/args
	std::vector<std::string> cmds = {"put",  "get",        "list",     "extendedlist",   "putdata",       "getdata", "listdata", "extendedlistdata",
	                                 "head", "deletefile", "deleteme", "extendedstatus", "notifications", "queue",   "dequeue"};
	// as every command works fine with too many args, we will simply pass three to all of them
	std::vector<std::string> args = {"arg1", "arg2", "arg3"};

	for (std::string cmd : cmds) {
		cm.initNotConnected();
		retval1 = cm.execute(cmd, args);
		cm.initConnected();
		retval2 = cm.execute(cmd, args);
		cm.initVersionChecked();
		retval3 = cm.execute(cmd, args);

		EXPECT_EQ(retval1.type, CmdMan::error);
		EXPECT_EQ(retval1.msg["command"].asString(), "error");
		EXPECT_NE(retval1.msg["error"].asString(), "");

		EXPECT_EQ(retval2.type, CmdMan::error);
		EXPECT_EQ(retval2.msg["command"].asString(), "error");
		EXPECT_NE(retval2.msg["error"].asString(), "");

		EXPECT_EQ(retval3.type, CmdMan::error);
		EXPECT_EQ(retval3.msg["command"].asString(), "error");
		EXPECT_NE(retval3.msg["error"].asString(), "");
	}
}

} // namespace
/* end of namespace */
