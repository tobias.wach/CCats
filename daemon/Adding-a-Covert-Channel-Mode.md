# Adding a Covert Channel Mode

There are multiple abstraction levels on which you can add a covert channel:
- CovertChannel
- BidirectionalChannels

## CovertChannel
The CovertChannel connects the inner network with the outer network. It forwards packets which are not important for the defined filter and can edit the filtered packets.
To do that a CovertChannel consists of 4 sniffers that run in their own threads.
- innerForward - forwards packets from inner network to outer network
- outerForward - forwards packets from outer network to inner network
- innerChannel - modifies filtered packets from inner network to outer network
- outerChannel - modifies filtered packets form outer network to inner network

To implement your own channel you should derive from CovertChannel.

### Deriving CovertChannel
The CovertChannel constructor needs the interface name of the inner and outer network and pcap filter strings to set the filters for the sniffer. The sniffers will call the handle functions when receiving a packet.

When you derive CovertChannel you must implement:
- handleChannelFromOuter
- handleChannelFromInner

The pdu parameter is the sniffed packet. This should be modified and send to the other (inner/outer) network.

#### ChannelControls
So you can control your channel ChannelControls is an interface to provide basic control elements. CovertChannel derives from ChannelControls so you have to implement the virtual functions and methods of ChannelControls as well.
- sendFile
- getProgress
- getTransferStart
- isTransferRunning
- reset
- getFileName

## BidirectionalChannels
Bidirectional channels are explicit TCP channels. It implements the basic controls from ChannelControls and sets the filter string for TCP connecitons.
So you can deriving from BidirectionalChannels and you must only implement:
- handleChannelFromOuter
- handleChannelFromInner
