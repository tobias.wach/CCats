import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Page {
    width: 1280
    height: 570
    id: logForm

    font.capitalization: Font.MixedCase

    Connections {
        target: _qmlHandler
        onLog: {
            log.append(new Date().toLocaleTimeString(Qt.locale("C"),
                                                     "[hh:mm:ss]\n") + logText)
            logFlickable.contentY = log.height - logFlickable.height
        }
    }

    Flickable {
        anchors.fill: parent
        id: logFlickable
        flickableDirection: Flickable.VerticalFlick
        Layout.preferredHeight: 170
        Layout.preferredWidth: parent.width

        TextArea.flickable: TextArea {
            selectByMouse: true
            id: log
            readOnly: true
            wrapMode: TextArea.Wrap
            text: qsTr("")
            font.pointSize: 15
        }

        ScrollBar.vertical: ScrollBar {
        }
    }
}
