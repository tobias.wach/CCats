#include <iostream>

#include "../include/Config.h"
#include "../include/CovertChannel/Channels/TCPAppendChannel.hpp"
#include "../include/CovertChannel/Channels/TCPOptionCustomChannel.hpp"
#include "../include/CovertChannel/Channels/TCPOptionTimestampChannel.hpp"
#include "../include/CovertChannel/Channels/TCPUrgencyChannel.hpp"
#include "../include/CovertChannel/CovertChannel.h"
#include "../include/CovertChannel/ForwardChannel.h"
#include "../include/Queue.h"
#include "../include/Server.h"
#include "../include/UserManager.h"

using namespace std;

/**
 * Creates the covert channel
 *
 * @return covert channel pointer | nullptr if no channel
 */
CovertChannel *createChannel() {
	CovertChannel *covertchannel = nullptr;

	const std::string covertChannelMode = Config::getValue("covertChannelMode");
	const string innerInterface = Config::getValue("innerInterface");
	const string outerInterface = Config::getValue("outerInterface");
	const string targetIP = Config::getValue("targetIP");
	const string targetPort = Config::getValue("targetPort");
	const string passiveMode = Config::getValue("passiveMode");

	if (covertChannelMode == "tcpurgency") {
		if (passiveMode == "true") {
			covertchannel = new TCPUrgencyChannel<true>(innerInterface, outerInterface, targetIP, targetPort);
		} else {
			covertchannel = new TCPUrgencyChannel<false>(innerInterface, outerInterface, targetIP, targetPort);
		}
		covertchannel->startSniffing();

	} else if (covertChannelMode == "tcpappend") {
		if (passiveMode == "true") {
			covertchannel = new TCPAppendChannel<8, true>(innerInterface, outerInterface, targetIP, targetPort);
		} else {
			covertchannel = new TCPAppendChannel<8, false>(innerInterface, outerInterface, targetIP, targetPort);
		}
		covertchannel->startSniffing();

	} else if (covertChannelMode == "tcpoptiontimestamp") {
		if (passiveMode == "true") {
			covertchannel = new TCPOptionTimestampChannel<true>(innerInterface, outerInterface, targetIP, targetPort);
		} else {
			covertchannel = new TCPOptionTimestampChannel<false>(innerInterface, outerInterface, targetIP, targetPort);
		}
		covertchannel->startSniffing();

	} else if (covertChannelMode == "tcpoptioncustom") {
		if (passiveMode == "true") {
			covertchannel = new TCPOptionCustomChannel<8, true>(innerInterface, outerInterface, targetIP, targetPort);
		} else {
			covertchannel = new TCPOptionCustomChannel<8, false>(innerInterface, outerInterface, targetIP, targetPort);
		}
		covertchannel->startSniffing();

	} else if (covertChannelMode == "forward") {
		covertchannel = new ForwardChannel(innerInterface, outerInterface);
		covertchannel->startSniffing();
	}

	return covertchannel;
}

int main(int argc, char *argv[]) {

	std::string configFile = "config.txt";
	if (argc >= 2) {
		if (std::string(argv[1]) == "help") {
			std::cout << "Usage " << argv[0] << " [config-file]" << std::endl;
			return 0;
		}

		configFile = argv[1];
	}

	// load config in namespace
	std::cout << "Loading config file \"" << configFile << "\"." << std::endl;
	if (!Config::init(configFile)) {
		cerr << "configuration could not be loaded properly" << endl;
		exit(EXIT_FAILURE);
	}

	CovertChannel *covertchannel = createChannel();

	// check if userStorage is add specified location
	// if not create one
	UserManager::init(Config::getValue("userdatabase"));

	// Init queue so the file transfers can be executed
	Queue::channel = covertchannel;

	try {
		io_service io_service;
		Server server(io_service);
		io_service.run();
	} catch (exception &e) {
		cerr << e.what() << endl;
	}

	if (covertchannel != nullptr) {
		delete (covertchannel);
	}

	return 0;
}
