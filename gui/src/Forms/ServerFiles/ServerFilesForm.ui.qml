import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

Page {
    width: 1280
    height: 570
    id: serverFilesForm

    font.capitalization: Font.MixedCase

    Connections {
        target: _qmlHandler
        onServerFilesListFile: {
            fileList.append({
                                "fileName": fileName,
                                "fileSize": fileSize + " kB",
                                "fileProgress": "",
                                "fileDecryptable": fileDecryptable,
                                "fileExistsLocally": existsLocally
                            })
        }

        onServerFilesClearFileList: {
            fileList.clear()
        }

        onServerFilesSetFileUrlText: {
            sendingSelectedFileText.text = signalText
        }

        onServerFilesEnableSendButton: {
            sendingSendFileButton.enabled = true
        }

        onServerFilesDisableSendButton: {
            sendingSendFileButton.enabled = false
        }
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.preferredWidth: 750
            Layout.preferredHeight: 30

            Text {

                text: qsTr("File Name")
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 400
                Layout.preferredHeight: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 15
            }

            Text {

                text: qsTr("Size")
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 15
            }

            Text {

                text: qsTr("Status")
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 15
            }

            Text {

                text: qsTr("Encryption")
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 150
                Layout.preferredHeight: parent.height
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 15
            }
        }

        Rectangle {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 2
            color: "#404040"
        }

        ScrollView {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.preferredWidth: parent.width

            Layout.preferredHeight: 440
            ListView {
                anchors.fill: parent
                model: fileList
                clip: true

                delegate: ServerFilesFileTemplate {
                    fileNameText: fileName
                    fileSizeText: fileSize
                    fileProgressText: fileProgress
                    fileDecryptableText: fileDecryptable
                    fileExists: fileExistsLocally
                }
            }
        }

        ListModel {
            id: fileList
        }

        Rectangle {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 2
            color: Material.accent
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredWidth: parent.width

            Text {

                text: qsTr("Selected File: ")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredWidth: 150
                Layout.preferredHeight: 50
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 23
            }

            Text {
                id: sendingSelectedFileText

                text: qsTr("None")
                Layout.preferredWidth: 450
                Layout.preferredHeight: 50
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 23
                elide: Text.ElideLeft
                ToolTip.visible: mouseArea.containsMouse
                ToolTip.text: text
                ToolTip.delay: 200

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                }
            }

            Button {
                id: sendingClearFileButton
                text: qsTr("Clear Selection")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.preferredWidth: 200
                Layout.preferredHeight: 50
                enabled: sendingSendFileButton.enabled
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onServerFilesClearSelectionButton()
                }
            }

            Button {
                id: sendingSelectFileButton
                text: qsTr("Select File")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.preferredWidth: 200
                Layout.preferredHeight: 50
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    sendingFileDialog.open()
                }
            }

            Button {
                id: sendingSendFileButton
                enabled: false
                text: qsTr("Upload File")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.preferredWidth: 200
                Layout.preferredHeight: 50
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onServerFilesSendFileButton()
                }
            }
        }
    }

    FileDialog {
        id: sendingFileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        // @disable-check M223
        onAccepted: {
            // @disable-check M222
            _qmlHandler.onServerFilesSelectFileButton(sendingFileDialog.fileUrl)
        }
    }
}
