#ifndef FORWARDCHANNEL_H
#define FORWARDCHANNEL_H

#include "CovertChannel.h"

/**
 * @class ForwardChannel
 *
 * A CovertChannel which forwards the traffic it captures.
 */
class ForwardChannel : public CovertChannel {
public:
	/**
	 * Sets up a CovertChannel.
	 *
	 * Creates a CovertChannel, sets the network interfaces for sniffing and sending and sets the filter.
	 *
	 * @param innerInterface name of the interface of the inner network
	 * @param outerInterface name of the interface of the outer network
	 * @param filter pcap filter string which will be set for the channel sniffers and negated for the forward sniffers
	 */
	ForwardChannel(const std::string &innerInterface, const std::string &outerInterface);

	/**
	 * Destroys the CovertChannel.
	 */
	virtual ~ForwardChannel();

	/* ChannelControls */

	/**
	 * Starts sending a file.
	 *
	 * Starts sending a file if no transmission is running and the file exists.
	 *
	 * @param fileName name of the file in the file directory
	 * @return true - file will be sent | false - file was not accepted
	 */
	virtual bool sendFile(const std::string &fileName);

	/**
	 * Get the progress
	 *
	 * @return progress counters
	 */
	virtual std::pair<uint32_t, uint32_t> getProgress();

	/**
	 * Test if a transfer is running
	 *
	 * @return true - a transfer runs | false - no transfer runs
	 */
	virtual bool isTransferRunning();

	/**
	 * Resets state and sets reset flag so a reset signal is sent in the next packet
	 */
	virtual void reset();

	/**
	 * Test if a transfer is running
	 *
	 * @return true - a transfer runs | false - no transfer runs
	 */
	virtual std::time_t getTransferStart();

	/**
	 * Get file name of the file which is currently be sent or received.
	 *
	 * @return file name
	 */
	virtual std::string getFileName();

	/* =============== */

protected:
	/**
	 * Handler for sniffed packets filterd to forward from the outer network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromOuter(Tins::PDU &pdu);

	/**
	 * Handler for sniffed packets filterd to forward from the inner network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromInner(Tins::PDU &pdu);
};

#endif
