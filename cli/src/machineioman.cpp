#include "../include/machineioman.h"

#include <iostream>
#include <vector>

MachineIoMan::MachineIoMan(bool usessl, const char *certfile, bool beverbose) : IoMan(usessl, certfile) { verbose = beverbose; }

void MachineIoMan::printMessage(std::string msg, OutMsgType type) {
	switch (type) {
	case normal: {
		std::cout << msg << std::endl;
		break;
	}
	case error: {
		std::cout << msg << std::endl;
		break;
	}
	case debug: {
		if (verbose)
			std::cerr << msg << std::endl;
		break;
	}
	}
}

void MachineIoMan::printWelcomeMessage() {}

std::string MachineIoMan::getCmdPrompt() { return ""; }
