FROM debian

RUN apt-get update -y && apt-get install -y \
    build-essential git cmake libpcap-dev libjsoncpp-dev wget libssl-dev

WORKDIR /root/build

RUN wget https://dl.bintray.com/boostorg/release/1.72.0/source/boost_1_72_0.tar.gz \
    && tar xfz boost_1_72_0.tar.gz \
    && rm boost_1_72_0.tar.gz \
    && cd boost_1_72_0 \
    && ./bootstrap.sh --prefix=/usr/local \
    && ./b2 -j$(nproc) install \
    && cd .. \
    && rm -rf boost_1_72_0

# Build and install libtins
RUN git clone https://github.com/mfontanini/libtins.git \
    && cd libtins && git checkout v4.2 \
    && mkdir build && cd build \
    && cmake ../ -DLIBTINS_ENABLE_CXX11=1 && make -j$(nproc) && make install \
    && cd ../.. && rm -rf libtins

# Build and install googletest
RUN git clone https://github.com/google/googletest.git \
    && cd googletest \
    && mkdir build && cd build \
    && cmake .. && make -j$(nproc) && make install \
    && cd ../.. && rm -rf googletest

RUN mkdir /root/build/files \
    && touch /root/build/config.txt \
    && echo "port=1234" >> /root/build/config.txt \
    && echo "interface=lo" >> /root/build/config.txt \
    && echo "userdatabase=userStorage.txt" >> /root/build/config.txt \
    && echo "filedirectory=./files/" >> /root/build/config.txt

# Copy all required data into image
COPY .cmake_modules/ /root/.cmake_modules
COPY libs/ /root/libs
COPY /daemon/include /root/build/include
COPY /daemon/src /root/build/src
COPY /daemon/test /root/build/test
COPY /daemon/CMakeLists.txt /root/build

# Compile ccats with tests
RUN cmake . && make -j$(nproc) \
    && rm -rf src include CMakeFiles \
    && rm Makefile CMakeCache.txt cmake_install.cmake CMakeLists.txt \
    CTestTestfile.cmake

# Set entrypoint and expose port
ENTRYPOINT /root/build/bin/ccats
EXPOSE 1234
