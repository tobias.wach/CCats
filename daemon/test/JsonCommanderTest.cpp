#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../include/Config.h"
#include "../include/JsonCommander.h"
#include "../include/Notifications.h"
#include "ChannelControlsMock.h"
#include "FileManagerMock.h"
#include "QueueMock.h"

namespace {
/* Version tests */
TEST(testVersion, PositiveAllEqual) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);
	Json::Value message;
	message["major"] = 0;
	message["minor"] = 1;

	JsonCommander::Response response = jsonCommander.testVersion(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["major"].asInt(), jsonCommander.protocolMajorVersion);
	EXPECT_EQ(response.json["minor"].asInt(), jsonCommander.protocolMinorVersion);
}

TEST(testVersion, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);
	Json::Value message;
	message["major"] = jsonCommander.protocolMajorVersion;
	message["minor"] = jsonCommander.protocolMinorVersion - 1;

	JsonCommander::Response response = jsonCommander.testVersion(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["major"].asInt(), jsonCommander.protocolMajorVersion);
	EXPECT_EQ(response.json["minor"].asInt(), jsonCommander.protocolMinorVersion);
}

TEST(testVersion, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);
	Json::Value message;
	message["major"] = "def";
	message["minor"] = "abc";

	JsonCommander::Response response = jsonCommander.testVersion(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["major"].asInt(), jsonCommander.protocolMajorVersion);
	EXPECT_EQ(response.json["minor"].asInt(), jsonCommander.protocolMinorVersion);
}

TEST(testVersion, NotEqualMajorNumber) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);
	Json::Value message;
	message["major"] = jsonCommander.protocolMajorVersion + 1;
	message["minor"] = jsonCommander.protocolMinorVersion;

	JsonCommander::Response response = jsonCommander.testVersion(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["major"].asInt(), jsonCommander.protocolMajorVersion);
	EXPECT_EQ(response.json["minor"].asInt(), jsonCommander.protocolMinorVersion);
}

TEST(testVersion, BiggerMinorNumber) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);
	Json::Value message;
	message["major"] = jsonCommander.protocolMajorVersion;
	message["minor"] = jsonCommander.protocolMinorVersion + 1;

	JsonCommander::Response response = jsonCommander.testVersion(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["major"].asInt(), jsonCommander.protocolMajorVersion);
	EXPECT_EQ(response.json["minor"].asInt(), jsonCommander.protocolMinorVersion);
}

/* Status tests */
TEST(Status, Ok) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "status";
	Json::Value message;
	message["command"] = command;

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["response"].asString(), "ok");
}

TEST(Status, Downloading) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "status";
	Json::Value message;
	message["command"] = command;

	ON_CALL(fileManager, isDownloading()).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["response"].asString(), "download running");
}

TEST(Status, Uploading) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "status";
	Json::Value message;
	message["command"] = command;

	ON_CALL(fileManager, isUploading()).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["response"].asString(), "upload running");
}

TEST(Status, UploadingAndDownloading) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "status";
	Json::Value message;
	message["command"] = command;

	ON_CALL(fileManager, isDownloading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, isUploading()).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["response"].asString(), "download and upload running");
}

/* Close tests */
TEST(Close, Close) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "close";
	Json::Value message;
	message["command"] = command;

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["response"].asString(), "bye");
}

/* Put tests */
TEST(Put, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "put";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;
	message["size"] = 1337;
	message["chunks"] = 1;

	ON_CALL(fileManager, openPutFile(testing::_)).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Put, Negative) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "put";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;
	message["size"] = 1337;
	message["chunks"] = 1;

	ON_CALL(fileManager, openPutFile(testing::_)).WillByDefault(testing::Return(false));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_TRUE(response.json["error"].asString().length() > 0);
}

/* Putdata tests */
TEST(Putdata, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	/* start with put */
	std::string command = "put";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;
	message["size"] = 1337;
	const int chunks = 3;
	message["chunks"] = chunks;

	ON_CALL(fileManager, openPutFile(testing::_)).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");

	/* putdata */
	command = "putdata";

	ON_CALL(fileManager, isUploading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, getPutBaseFileName()).WillByDefault(testing::Return(filename));

	for (int remaining = chunks - 1; remaining >= 0; remaining--) {
		message = Json::Value();
		message["command"] = command;
		message["file"] = filename;
		message["data"] = "MTMzNw==";
		message["remaining"] = remaining;
		message["cancel"] = false;

		response = jsonCommander.execute(message);

		EXPECT_TRUE(response.action == JsonCommander::Action::send);
		EXPECT_EQ(response.json["command"].asString(), command);
		EXPECT_FALSE(response.json["cancel"].asBool());
		EXPECT_EQ(response.json["received"].asInt(), remaining);
		EXPECT_EQ(response.json["file"].asString(), filename);
		EXPECT_EQ(response.json["error"].asString(), "");
	}
}

TEST(Putdata, Cancel) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	/* start with put */
	std::string command = "put";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;
	message["size"] = 1337;
	const int chunks = 3;
	message["chunks"] = chunks;

	ON_CALL(fileManager, openPutFile(testing::_)).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");

	/* putdata */
	command = "putdata";

	ON_CALL(fileManager, isUploading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, getPutBaseFileName()).WillByDefault(testing::Return(filename));

	int remaining = chunks - 1;
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["data"] = "MTMzNw==";
	message["remaining"] = remaining;
	message["cancel"] = false;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["received"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");

	// cancel transfer
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["data"] = "MTMzNw==";
	message["remaining"] = --remaining;
	message["cancel"] = true;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["received"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Putdata, WrongRemaining) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	/* start with put */
	std::string command = "put";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;
	message["size"] = 1337;
	const int chunks = 3;
	message["chunks"] = chunks;

	ON_CALL(fileManager, openPutFile(testing::_)).WillByDefault(testing::Return(true));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");

	/* putdata */
	command = "putdata";

	ON_CALL(fileManager, isUploading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, getPutBaseFileName()).WillByDefault(testing::Return(filename));

	int remaining = chunks - 1;
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["data"] = "MTMzNw==";
	message["remaining"] = remaining;
	message["cancel"] = false;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["received"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["error"].asString(), "");
	message = Json::Value();

	// skip remaining=1 and provoke an error
	remaining = 0;
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["data"] = "MTMzNw==";
	message["remaining"] = remaining;
	message["cancel"] = false;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["received"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_TRUE(response.json["error"].asString().length() > 0);
}

/* Get tests */
TEST(Get, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "get";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;

	const int chunks = 3;
	EXPECT_CALL(fileManager, openGetFile(testing::_)).WillOnce(testing::Return(std::pair<bool, int>(true, chunks)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["chunks"].asInt(), chunks);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Get, Negative) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "get";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;

	EXPECT_CALL(fileManager, openGetFile(testing::_)).WillOnce(testing::Return(std::pair<bool, int>(false, -1)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["chunks"].asInt(), -1);
	EXPECT_TRUE(response.json["error"].asString().length() > 0);
}

/* Getdata tests */
TEST(Getdata, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	std::string command = "get";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;

	const int chunks = 3;
	EXPECT_CALL(fileManager, openGetFile(testing::_)).WillOnce(testing::Return(std::pair<bool, int>(true, chunks)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["chunks"].asInt(), chunks);
	EXPECT_EQ(response.json["error"].asString(), "");

	/* getdata */
	command = "getdata";

	ON_CALL(fileManager, isDownloading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, getGetBaseFileName()).WillByDefault(testing::Return(filename));

	std::vector<char> data;
	data.push_back('1');
	data.push_back('3');
	data.push_back('3');
	data.push_back('7');

	ON_CALL(fileManager, readGet()).WillByDefault(testing::Return(data));

	for (int remaining = chunks - 1; remaining >= 0; remaining--) {
		message = Json::Value();
		message["command"] = command;
		message["file"] = filename;
		message["chunk"] = remaining;
		message["cancel"] = false;

		response = jsonCommander.execute(message);

		EXPECT_TRUE(response.action == JsonCommander::Action::send);
		EXPECT_EQ(response.json["command"].asString(), command);
		EXPECT_FALSE(response.json["cancel"].asBool());
		EXPECT_EQ(response.json["remaining"].asInt(), remaining);
		EXPECT_EQ(response.json["file"].asString(), filename);
		EXPECT_EQ(response.json["data"].asString(), "MTMzNw==");
		EXPECT_EQ(response.json["error"].asString(), "");
	}
}

TEST(Getdata, Cancle) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	std::string command = "get";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;

	const int chunks = 3;
	EXPECT_CALL(fileManager, openGetFile(testing::_)).WillOnce(testing::Return(std::pair<bool, int>(true, chunks)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["chunks"].asInt(), chunks);
	EXPECT_EQ(response.json["error"].asString(), "");

	/* getdata */
	command = "getdata";

	ON_CALL(fileManager, isDownloading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, getGetBaseFileName()).WillByDefault(testing::Return(filename));

	std::vector<char> data;
	data.push_back('1');
	data.push_back('3');
	data.push_back('3');
	data.push_back('7');

	ON_CALL(fileManager, readGet()).WillByDefault(testing::Return(data));

	int remaining = chunks - 1;
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["chunk"] = remaining;
	message["cancel"] = false;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["data"].asString(), "MTMzNw==");
	EXPECT_EQ(response.json["error"].asString(), "");

	// set cancel to true
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["chunk"] = --remaining;
	message["cancel"] = true;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["data"].asString(), "");
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Getdata, WrongChunk) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	std::string command = "get";
	const std::string filename = "cool.txt";
	Json::Value message;
	message["command"] = command;
	message["file"] = filename;

	const int chunks = 3;
	EXPECT_CALL(fileManager, openGetFile(testing::_)).WillOnce(testing::Return(std::pair<bool, int>(true, chunks)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["chunks"].asInt(), chunks);
	EXPECT_EQ(response.json["error"].asString(), "");

	/* getdata */
	command = "getdata";

	ON_CALL(fileManager, isDownloading()).WillByDefault(testing::Return(true));
	ON_CALL(fileManager, getGetBaseFileName()).WillByDefault(testing::Return(filename));

	std::vector<char> data;
	data.push_back('1');
	data.push_back('3');
	data.push_back('3');
	data.push_back('7');

	ON_CALL(fileManager, readGet()).WillByDefault(testing::Return(data));

	int remaining = chunks - 1;
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["chunk"] = remaining;
	message["cancel"] = false;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["data"].asString(), "MTMzNw==");
	EXPECT_EQ(response.json["error"].asString(), "");

	// skip chunk=0
	remaining = 0;
	message = Json::Value();
	message["command"] = command;
	message["file"] = filename;
	message["chunk"] = remaining;
	message["cancel"] = false;

	response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), remaining);
	EXPECT_EQ(response.json["file"].asString(), filename);
	EXPECT_EQ(response.json["data"].asString(), "");
	EXPECT_TRUE(response.json["error"].asString().length() > 0);
}

/* List tests */
TEST(List, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "list";
	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, openList()).WillOnce(testing::Return(1));
	EXPECT_CALL(fileManager, getListSize()).WillOnce(testing::Return(5));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["chunks"].asInt(), 1);
	EXPECT_EQ(response.json["items"].asInt(), 5);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(List, Negative) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "list";
	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, openList()).WillOnce(testing::Return(-1));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["chunks"].asInt(), -1);
	EXPECT_EQ(response.json["items"].asInt(), -1);
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(List, EmptyList) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "list";
	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, openList()).WillOnce(testing::Return(0));
	EXPECT_CALL(fileManager, getListSize()).WillOnce(testing::Return(0));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["chunks"].asInt(), 0);
	EXPECT_EQ(response.json["items"].asInt(), 0);
	EXPECT_EQ(response.json["error"].asString(), "");
}

/* Listdata tests */

void fillExampleFileList(std::vector<std::string> (&chunk)[3]) {
	chunk[0].push_back("file01.txt");
	chunk[0].push_back("bumdibumps");
	chunk[0].push_back("1");
	chunk[0].push_back("Ich habe Hunger.txt");
	chunk[0].push_back("answerIs42");
	chunk[0].push_back("123456789456115811");
	chunk[0].push_back("kek");
	chunk[1].push_back("1337");
	chunk[1].push_back("cats.png");
	chunk[1].push_back("more_cats.png");
	chunk[1].push_back("ugly dog.tiff");
	chunk[1].push_back("hello.txt");
	chunk[1].push_back("bye.exe");
	chunk[1].push_back("poster.pdf");
	chunk[2].push_back("headbang.gif");
	chunk[2].push_back("feelsbad.jpg");
	chunk[2].push_back("hack.s");
	chunk[2].push_back("SodiumChloride");
	chunk[2].push_back("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrst"
	                   "uvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN"
	                   "OPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
}

TEST(Listdata, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "listdata";

	const int chunks = 3;
	std::vector<std::string> chunk[chunks];
	fillExampleFileList(chunk);

	int remaining = chunks - 1;

	for (int k = 0; k < chunks; k++) {
		Json::Value message;
		message["command"] = command;
		message["chunk"] = remaining;
		message["cancel"] = false;

		EXPECT_CALL(fileManager, getRemainingListChunks()).WillOnce(testing::Return(remaining + 1));
		EXPECT_CALL(fileManager, getNextChunkFromList()).WillOnce(testing::Return(chunk[k]));

		JsonCommander::Response response = jsonCommander.execute(message);

		EXPECT_TRUE(response.action == JsonCommander::Action::send);
		EXPECT_EQ(response.json["command"].asString(), command);
		EXPECT_FALSE(response.json["cancel"].asBool());
		EXPECT_EQ(response.json["remaining"].asInt(), remaining--);
		EXPECT_TRUE(response.json["names"].isArray());
		Json::Value array = response.json["names"];
		EXPECT_EQ(array.size(), chunk[k].size());
		for (int i = 0; i < 3; i++) {
			EXPECT_EQ(array[i].asString(), chunk[k][i]);
		}
		EXPECT_EQ(response.json["error"].asString(), "");
	}
}

TEST(Listdata, Cancel) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "listdata";

	const int chunks = 3;
	std::vector<std::string> chunk[chunks];
	fillExampleFileList(chunk);

	int remaining = chunks - 1;

	Json::Value message;
	message["command"] = command;
	message["chunk"] = remaining;
	message["cancel"] = false;

	EXPECT_CALL(fileManager, getRemainingListChunks()).WillOnce(testing::Return(remaining + 1));
	EXPECT_CALL(fileManager, getNextChunkFromList()).WillOnce(testing::Return(chunk[0]));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), remaining--);
	EXPECT_TRUE(response.json["names"].isArray());
	Json::Value array = response.json["names"];
	EXPECT_EQ(array.size(), chunk[0].size());
	for (int i = 0; i < 3; i++) {
		EXPECT_EQ(array[i].asString(), chunk[0][i]);
	}
	EXPECT_EQ(response.json["error"].asString(), "");

	message = Json::Value();
	message["command"] = command;
	message["chunk"] = remaining;
	message["cancel"] = true;

	EXPECT_CALL(fileManager, getRemainingListChunks()).WillOnce(testing::Return(remaining + 1));

	response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), remaining--);
	EXPECT_TRUE(response.json["names"].isArray());
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Listdata, WrongChunkNumber) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "listdata";

	const int chunks = 3;
	int remaining = chunks - 1;

	Json::Value message;
	message["command"] = command;
	message["chunk"] = remaining;
	message["cancel"] = false;

	// return smaller remaining
	EXPECT_CALL(fileManager, getRemainingListChunks()).WillOnce(testing::Return(remaining));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), -1);
	EXPECT_TRUE(response.json["names"].isArray());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Listdata, NoChunksToBeSend) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "listdata";

	const int chunks = 0;

	Json::Value message;
	message["command"] = command;
	message["chunk"] = 1;
	message["cancel"] = false;

	// return smaller remaining
	EXPECT_CALL(fileManager, getRemainingListChunks()).WillOnce(testing::Return(chunks));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), -1);
	EXPECT_TRUE(response.json["names"].isArray());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Listdata, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "listdata";

	const int chunks = 3;

	Json::Value message;
	message["command"] = command;
	message["chunk"] = 1;

	// return smaller remaining
	EXPECT_CALL(fileManager, getRemainingListChunks()).WillOnce(testing::Return(chunks));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), -1);
	EXPECT_TRUE(response.json["names"].isArray());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Head, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "head";
	const std::string file = "asdf.txt";

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	std::vector<char> bytes = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	const std::string bytesAsString = "YWJjZGVmZ2g=";
	EXPECT_CALL(fileManager, getBytesFromFile(testing::_, testing::_)).WillOnce(testing::Return(std::make_pair(bytes, FileManager::Error::no_error)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), file);
	EXPECT_EQ(response.json["data"].asString(), bytesAsString);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Head, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "head";
	const int file = 3641;

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "");
	EXPECT_EQ(response.json["data"].asString(), "");
	EXPECT_NE(response.json["error"].asString(), "");
}

TEST(Head, NoSuchFile) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "head";
	const std::string file = "asdf.txt";

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	std::vector<char> bytes;
	EXPECT_CALL(fileManager, getBytesFromFile(testing::_, testing::_)).WillOnce(testing::Return(std::make_pair(bytes, FileManager::Error::no_such_file)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), file);
	EXPECT_EQ(response.json["data"].asString(), "");
	EXPECT_NE(response.json["error"].asString(), "");
}

TEST(Head, FileTooSmall) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "head";
	const std::string file = "asdf.txt";

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	std::vector<char> bytes;
	EXPECT_CALL(fileManager, getBytesFromFile(testing::_, testing::_))
	    .Times(2)
	    .WillRepeatedly(testing::Return(std::make_pair(bytes, FileManager::Error::file_too_small)));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), file);
	EXPECT_EQ(response.json["data"].asString(), "");
	EXPECT_NE(response.json["error"].asString(), "");
}

TEST(Deleteme, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	// need to set currentUser in jsonCommander via calling checkLogin
	Json::Value login;
	login["login"] = true;
	login["user"] = "positive";
	login["pass"] = "positive";
	login["cancel"] = false;

	JsonCommander::Response loginRes = jsonCommander.checkLogin(login);
	EXPECT_TRUE(loginRes.json["accept"].asBool());
	EXPECT_EQ(loginRes.json["error"].asString(), "");

	// now the actual test
	const std::string command = "deleteme";

	Json::Value message;
	message["command"] = command;
	message["pass"] = "positive";

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_EQ(response.action, JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Deleteme, Negative) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	// need to set currentUser in jsonCommander via calling checkLogin
	Json::Value login;
	login["login"] = true;
	login["user"] = "positive";
	login["pass"] = "positive";
	login["cancel"] = false;

	JsonCommander::Response loginRes = jsonCommander.checkLogin(login);
	EXPECT_TRUE(loginRes.json["accept"].asBool());
	EXPECT_EQ(loginRes.json["error"].asString(), "");

	// now the actual test
	const std::string command = "deleteme";

	Json::Value message;
	message["command"] = command;
	message["pass"] = "negative";

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_EQ(response.action, JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_NE(response.json["error"].asString(), "");
}

TEST(Deleteme, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	// need to set currentUser in jsonCommander via calling checkLogin
	Json::Value login;
	login["login"] = true;
	login["user"] = "positive";
	login["pass"] = "positive";
	login["cancel"] = false;

	JsonCommander::Response loginRes = jsonCommander.checkLogin(login);
	EXPECT_TRUE(loginRes.json["accept"].asBool());
	EXPECT_EQ(loginRes.json["error"].asString(), "");

	// now the actual test
	const std::string command = "deleteme";

	Json::Value message;
	message["command"] = command;
	message["pass"] = 123;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_EQ(response.action, JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_NE(response.json["error"].asString(), "");
}

TEST(DeleteFile, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "deletefile";
	const std::string file = "asdf.txt";

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	EXPECT_CALL(fileManager, deleteFile(file)).WillOnce(testing::Return(FileManager::Error::no_error));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["file"].asString(), file);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(DeleteFile, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "deletefile";
	const int file = 3641;

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["file"].asString(), "");
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(DeleteFile, FileDoesNotExist) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "deletefile";
	const std::string file = "asdf.txt";

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	EXPECT_CALL(fileManager, deleteFile(file)).WillOnce(testing::Return(FileManager::Error::no_such_file));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["file"].asString(), file);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(DeleteFile, DisabledInConfig) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "deletefile";
	const std::string file = "asdf.txt";

	Json::Value message;
	message["command"] = command;
	message["file"] = file;

	EXPECT_CALL(fileManager, deleteFile(file)).WillOnce(testing::Return(FileManager::Error::not_allowed));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_EQ(response.json["file"].asString(), file);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(ExtendedStatus, NoUploadOrDownload) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(false));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(false));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(false));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_TRUE(response.json["transfersclientserver"].isNull());
	EXPECT_TRUE(response.json["transfersserverserver"].isNull());
}

TEST(ExtendedStatus, ClientServerDownload) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(false));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(true));
	EXPECT_CALL(fileManager, getGetBaseFileName()).WillOnce(testing::Return("asdf"));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(false));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_EQ(response.json["transfersclientserver"].size(), 1);
	EXPECT_FALSE(response.json["transfersclientserver"][0]["upload"].asBool());
	EXPECT_EQ(response.json["transfersclientserver"][0]["file"].asString(), "asdf");
	EXPECT_TRUE(response.json["transfersclientserver"][0]["progress"].isInt());
}

TEST(ExtendedStatus, ClientServerUpload) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(true));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(false));
	EXPECT_CALL(fileManager, getPutBaseFileName()).WillOnce(testing::Return("asdf"));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(false));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_EQ(response.json["transfersclientserver"].size(), 1);
	EXPECT_TRUE(response.json["transfersclientserver"][0]["upload"].asBool());
	EXPECT_EQ(response.json["transfersclientserver"][0]["file"].asString(), "asdf");
	EXPECT_TRUE(response.json["transfersclientserver"][0]["progress"].isInt());
}

TEST(ExtendedStatus, ClientServerDonwloadAndUpload) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(true));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(true));
	EXPECT_CALL(fileManager, getGetBaseFileName()).WillOnce(testing::Return("asdfGet"));
	EXPECT_CALL(fileManager, getPutBaseFileName()).WillOnce(testing::Return("asdfPut"));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(false));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_EQ(response.json["transfersclientserver"].size(), 2);
	EXPECT_TRUE(response.json["transfersclientserver"][0]["upload"].asBool());
	EXPECT_EQ(response.json["transfersclientserver"][0]["file"].asString(), "asdfPut");
	EXPECT_TRUE(response.json["transfersclientserver"][0]["progress"].isInt());
	EXPECT_FALSE(response.json["transfersclientserver"][1]["upload"].asBool());
	EXPECT_EQ(response.json["transfersclientserver"][1]["file"].asString(), "asdfGet");
	EXPECT_TRUE(response.json["transfersclientserver"][1]["progress"].isInt());
}

TEST(ExtendedStatus, ServerServerDownload) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();
	Queue::fileName = "a.txt";
	Config::storage.clear();
	Config::storage.insert(std::pair<std::string, std::string>("passiveMode", "true"));
	Config::storage.insert(std::pair<std::string, std::string>("covertChannelMode", "m"));

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(false));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(false));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(true));
	EXPECT_CALL(channelControls, getProgress()).WillOnce(testing::Return(std::pair<int, int>(1, 2)));
	EXPECT_CALL(channelControls, getTransferStart()).WillOnce(testing::Return(0));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_EQ(response.json["transfersclientserver"].size(), 0);
	EXPECT_EQ(response.json["transfersserverserver"].size(), 1);
	EXPECT_EQ(response.json["transfersserverserver"][0]["type"].asString(), "download");
	EXPECT_EQ(response.json["transfersserverserver"][0]["file"].asString(), "a.txt");
	EXPECT_EQ(response.json["transfersserverserver"][0]["progress"].asInt(), 50);
	EXPECT_TRUE(response.json["transfersserverserver"][0]["speed"].isDouble());
	EXPECT_EQ(response.json["transfersserverserver"][0]["method"].asString(), "m");
}

TEST(ExtendedStatus, ServerServerUpload) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();
	Queue::fileName = "a.txt";
	Config::storage.clear();
	Config::storage.insert(std::pair<std::string, std::string>("passiveMode", "false"));
	Config::storage.insert(std::pair<std::string, std::string>("covertChannelMode", "m"));

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(false));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(false));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(true));
	EXPECT_CALL(channelControls, getProgress()).WillOnce(testing::Return(std::pair<int, int>(1, 2)));
	EXPECT_CALL(channelControls, getTransferStart()).WillOnce(testing::Return(0));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_EQ(response.json["transfersclientserver"].size(), 0);
	EXPECT_EQ(response.json["transfersserverserver"].size(), 1);
	EXPECT_EQ(response.json["transfersserverserver"][0]["type"].asString(), "upload");
	EXPECT_EQ(response.json["transfersserverserver"][0]["file"].asString(), "a.txt");
	EXPECT_EQ(response.json["transfersserverserver"][0]["progress"].asInt(), 50);
	EXPECT_TRUE(response.json["transfersserverserver"][0]["speed"].isDouble());
	EXPECT_EQ(response.json["transfersserverserver"][0]["method"].asString(), "m");
}

TEST(ExtendedStatus, QueueNotEmpty) {
	FileManagerMock fileManager;
	JsonCommander jsonCommander(fileManager);
	ChannelControlsMock channelControls;
	Queue::channel = &channelControls;
	Queue::queue.clear();
	Queue::queue.push_back("a");
	Queue::queue.push_back("b");
	Config::storage.clear();
	Config::storage.insert(std::pair<std::string, std::string>("covertChannelMode", "m"));

	const std::string command = "extendedstatus";

	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, isUploading()).WillOnce(testing::Return(false));
	EXPECT_CALL(fileManager, isDownloading()).WillOnce(testing::Return(false));
	EXPECT_CALL(channelControls, isTransferRunning()).WillOnce(testing::Return(false));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_EQ(response.json["transfersclientserver"].size(), 0);
	EXPECT_EQ(response.json["transfersserverserver"].size(), 2);
	EXPECT_EQ(response.json["transfersserverserver"][0]["type"], "queued");
	EXPECT_EQ(response.json["transfersserverserver"][0]["file"], "a");
	EXPECT_EQ(response.json["transfersserverserver"][0]["progress"], 0);
	EXPECT_EQ(response.json["transfersserverserver"][0]["speed"], 0);
	EXPECT_EQ(response.json["transfersserverserver"][0]["method"], "m");
	EXPECT_EQ(response.json["transfersserverserver"][1]["type"], "queued");
	EXPECT_EQ(response.json["transfersserverserver"][1]["file"], "b");
	EXPECT_EQ(response.json["transfersserverserver"][1]["progress"], 0);
	EXPECT_EQ(response.json["transfersserverserver"][1]["speed"], 0);
	EXPECT_EQ(response.json["transfersserverserver"][1]["method"], "m");
}

TEST(Notifications, NoMessage) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "notifications";

	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();

	Json::Value message;
	message["command"] = command;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_TRUE(response.json["messages"].isNull());
}

TEST(Notifications, OneMessage) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "notifications";

	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();
	Notifications::newNotification("asdf");

	Json::Value message;
	message["command"] = command;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_TRUE(response.json["messages"].isArray());
	EXPECT_EQ(response.json["messages"][0], "asdf");

	// cleaning up
	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();
}

TEST(Notifications, OneMessageMultipleTimesCalled) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "notifications";

	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();
	Notifications::newNotification("asdf");

	Json::Value message;
	message["command"] = command;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_TRUE(response.json["messages"].isArray());
	EXPECT_EQ(response.json["messages"][0], "asdf");

	// make suhre that timestamp from message is older
	Notifications::messages.at(0).first--;

	response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_TRUE(response.json["messages"].isNull());

	// cleaning up
	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();
}

TEST(Notifications, TwoMessages) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "notifications";

	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();
	Notifications::newNotification("asdf");
	Notifications::newNotification("qwer");

	Json::Value message;
	message["command"] = command;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["error"].asString(), "");
	EXPECT_TRUE(response.json["messages"].isArray());
	EXPECT_EQ(response.json["messages"][0], "asdf");
	EXPECT_EQ(response.json["messages"][1], "qwer");

	// cleaning up
	Notifications::messages.clear();
	Notifications::userTimeStamps.clear();
}

TEST(ExtendedList, positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlist";
	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(0));
	EXPECT_CALL(fileManager, openExtendedList()).WillOnce(testing::Return(1));
	EXPECT_CALL(fileManager, getExtendedListSize()).WillOnce(testing::Return(5));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["chunks"].asInt(), 1);
	EXPECT_EQ(response.json["items"].asInt(), 5);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(ExtendedList, negative) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlist";
	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(0));
	EXPECT_CALL(fileManager, openExtendedList()).WillOnce(testing::Return(-1));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["chunks"].asInt(), -1);
	EXPECT_EQ(response.json["items"].asInt(), -1);
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(ExtendedList, EmptyList) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlist";
	Json::Value message;
	message["command"] = command;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(0));
	EXPECT_CALL(fileManager, openExtendedList()).WillOnce(testing::Return(0));
	EXPECT_CALL(fileManager, getExtendedListSize()).WillOnce(testing::Return(0));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["chunks"].asInt(), 0);
	EXPECT_EQ(response.json["items"].asInt(), 0);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(ExtendedListData, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlistdata";
	Json::Value message;
	message["command"] = command;
	message["chunk"] = 0;
	message["cancel"] = false;

	std::vector<std::tuple<std::string, std::string, double>> chunks;
	std::tuple<std::string, std::string, double> chunk = std::tuple("asdf", "asdfghjkasdfghjkasdfghjkasdfghjk", 41.2);
	chunks.push_back(chunk);
	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(1));
	EXPECT_CALL(fileManager, getNextChunkFromExtendedList()).WillOnce(testing::Return(chunks));

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), 0);
	EXPECT_TRUE(response.json["files"].isArray());
	EXPECT_EQ(response.json["files"].size(), 1);
	EXPECT_EQ(response.json["files"][0]["name"].asString(), "asdf");
	EXPECT_NE(response.json["files"][0]["head"].asString(), "");
	EXPECT_EQ(response.json["files"][0]["size"].asDouble(), 41.2);
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(ExtendedListData, Cancel) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlistdata";
	Json::Value message;
	message["command"] = command;
	message["chunk"] = 0;
	message["cancel"] = true;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(1));
	EXPECT_CALL(fileManager, cancelExtendedList());

	JsonCommander::Response response = jsonCommander.execute(message);

	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), 0);
	EXPECT_TRUE(response.json["files"].isArray());
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(ExtendedListData, WrongChunkNumber) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlistdata";
	Json::Value message;
	message["command"] = command;
	message["chunk"] = 1;
	message["cancel"] = false;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(1));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), -1);
	EXPECT_TRUE(response.json["files"].isArray());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(ExtendedListData, NoChunksToBeSend) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlistdata";
	Json::Value message;
	message["command"] = command;
	message["chunk"] = 1;
	message["cancel"] = false;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(0));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), -1);
	EXPECT_TRUE(response.json["files"].isArray());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(ExtendedListData, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "extendedlistdata";
	Json::Value message;
	message["command"] = command;
	message["chunk"] = 5;

	EXPECT_CALL(fileManager, getRemainingExtendedListChunks()).WillOnce(testing::Return(0));

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["cancel"].asBool());
	EXPECT_EQ(response.json["remaining"].asInt(), -1);
	EXPECT_TRUE(response.json["files"].isArray());
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Queue, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "queue";
	Json::Value message;
	message["command"] = command;
	message["file"] = 5;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "");
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Queue, PushToQueueFailed) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "queue";
	Json::Value message;
	message["command"] = command;
	message["file"] = "negative";

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "negative");
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Queue, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "queue";
	Json::Value message;
	message["command"] = command;
	message["file"] = "positive";

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "positive");
	EXPECT_EQ(response.json["error"].asString(), "");
}

TEST(Dequeue, InvalidRequest) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "dequeue";
	Json::Value message;
	message["command"] = command;
	message["file"] = 5;

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::closeAndSend);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "");
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Dequeue, QueueRemoveFailed) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "dequeue";
	Json::Value message;
	message["command"] = command;
	message["file"] = "negative";

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_FALSE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "negative");
	EXPECT_TRUE(response.json["error"].asString().compare("") != 0);
}

TEST(Dequeue, Positive) {
	FileManagerMock fileManager;

	JsonCommander jsonCommander(fileManager);

	const std::string command = "dequeue";
	Json::Value message;
	message["command"] = command;
	message["file"] = "positive";

	JsonCommander::Response response = jsonCommander.execute(message);
	EXPECT_TRUE(response.action == JsonCommander::Action::send);
	EXPECT_EQ(response.json["command"].asString(), command);
	EXPECT_TRUE(response.json["accept"].asBool());
	EXPECT_EQ(response.json["file"].asString(), "positive");
	EXPECT_EQ(response.json["error"].asString(), "");
}

} // namespace
