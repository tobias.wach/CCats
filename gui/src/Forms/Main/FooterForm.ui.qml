import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Page {
    width: 1280
    height: 70

    font.capitalization: Font.MixedCase

    Connections {
        target: _qmlHandler
        onFooterSetStatus: {
            footerStatusText.text = status
        }

        onFooterSetError: {
            footerErrorText.text = "Error: " + error
            timer.start()
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#c4c4c4"

        ColumnLayout {
            spacing: 3
            anchors.fill: parent

            Text {
                id: footerStatusText
                Layout.preferredWidth: parent.width

                text: qsTr("")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 23
            }

            Text {
                id: footerErrorText
                Layout.preferredWidth: parent.width
                color: "#fa3737"
                text: qsTr("")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 23
            }
        }
    }

    Timer {
        id: timer
        interval: 5000
        onTriggered: {
            footerErrorText.text = ""
        }
    }
}
