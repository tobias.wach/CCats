#ifndef QMLHANDLER_H
#define QMLHANDLER_H

#include <QObject>
#include <QUrl>

extern bool _RESTART;

/**
 * @class QMLHandler
 *
 * This class is the connection between Qml and C++, where signals can be emitted using code to interact with the forms
 * and slots can be used to run code from a Qml form.
 */
class QMLHandler : public QObject {
	Q_OBJECT

public:
	/**
	 * This class
	 */
	explicit QMLHandler(QObject *parent = 0);

	/**
	 * Handle the closing of the main window
	 */
	void onExit();

	/**
	 * Get the settings from the config class and adjust the gui settings accordingly
	 */
	void loadSettingsToGUI();

	/**
	 * Set the _RESTART variable
	 * @param restart The new value
	 */
	void setRestart(bool restart);

	/**
	 * Set the configExists variable
	 * @param exists The new value
	 */
	void setConfigExists(bool exists);

	// C++ -> QML
signals:
	// No Config Found Popup

	/**
	 * Open the popup for when no config has been found
	 */
	void noConfigFoundPopupOpen();
	/**
	 * Close the popup for when no config has been found
	 */
	void noConfigFoundPopupClose();

	// Invalid Cli Path Popup

	/**
	 * Open the popup for when the cli path is not valid
	 */
	void invalidCliPathPopupOpen();
	/**
	 * Close the popup for when the cli path is not valid
	 */
	void invalidCliPathPopupClose();

	// Invalid Config Popup

	/**
	 * Open the popup for when the config is invalid
	 */
	void invalidConfigPopupOpen();
	/**
	 * Close the popup for when the config is invalid
	 */
	void invalidConfigPopupClose();

	// Server Files

	/**
	 * Set the text that shows the selected file to be uploaded
	 * @param signalText The new file url
	 */
	void serverFilesSetFileUrlText(QString signalText);
	/**
	 * Enable the Upload File button
	 */
	void serverFilesEnableSendButton();
	/**
	 * Disable the Upload File button
	 */
	void serverFilesDisableSendButton();
	/**
	 * Clear the list of files that are on the server
	 */
	void serverFilesClearFileList();
	/**
	 * List a new file in the file list
	 * @param fileName The name of the file
	 * @param fileSize The size of the file
	 * @param fileDecryptable Is the file decryptable for the current user?
	 * @param existsLocally Is the file already downloaded?
	 */
	void serverFilesListFile(QString fileName, QString fileSize, QString fileDecryptable, bool existsLocally);
	/**
	 * Update an existing file in the file list
	 * @param fileName The name of the file
	 * @param fileProgress The download or upload progress of the file
	 * @param isQueued Is the file queued on the server?
	 */
	void serverFilesUpdateFile(QString fileName, QString fileProgress, bool isQueued);
	/**
	 * Disable the download button for a specific file
	 * @param The name of the file
	 */
	void serverFilesDisableDownloadButton(QString fileName);
	/**
	 * Close the delete popup for a file
	 */
	void serverFilesCloseConfirmDeletePopup();

	// Settings

	/**
	 * Close the main window
	 */
	void closeWindow();
	/**
	 * Load the settings to the GUI
	 * @param saveIP Should the autofill ip toggle be switched on or off?
	 * @param saveUsername Should the autofill username toggle be switched on or off?
	 * @param cliPath The path to the cli
	 * @param keyPath The path to the keyfile
	 */
	void loadSettings(bool saveIP, bool saveUsername, QString cliPath, QString keyPath);
	/**
	 * Update the status of the keyfile
	 * @param success Has the keyfile successfully been loaded?
	 * @param msg The message to be shown
	 */
	void keyfileStatus(bool success, QString msg);
	/**
	 * Signal for when the keyfile has been successfully closed and should be shown in the settings
	 */
	void keyfileClosedOK();

	// Delete Me Popup

	/**
	 * Set the status on the delete me popup (when a user want's to delete their acccount)
	 * @param status The status message
	 */
	void deleteMePopupSetStatus(QString status);

	// Ip Popup

	/**
	 * Set the status on the ip popup
	 * @param status The status messsage
	 */
	void ipPopupSetStatus(QString status);
	/**
	 * Set the ip address in the ip input field (used for autofill ip)
	 * @param default_ip The ip address
	 */
	void ipPopupSetIP(QString default_ip);
	/**
	 * Close the ip popup
	 */
	void ipPopupClose();
	/**
	 * Open the ip popup
	 */
	void ipPopupOpen();
	/**
	 * Enable the connect button in the ip popup
	 */
	void ipPopupEnableConnectButton();
	/**
	 * Disbale the connect button in the ip popup
	 */
	void ipPopupDisableConnectButton();
	/**
	 * Tick the "Save IP" checkbox in the ip popup
	 */
	void ipPopupCheckSaveCheckbox();

	// Login Signup Popup

	/**
	 * Close the login/signup popup
	 */
	void loginSignupPopupClose();
	/**
	 * Open the login/signup popup
	 */
	void loginSignupPopupOpen();
	/**
	 * Tick the "Save Username" checkbox in the login/signup popup
	 */
	void loginSignupCheckSaveCheckbox();

	// Login

	/**
	 * Set the status for the login tab in the login/signup popup
	 * @param status The status message
	 */
	void loginSetStatus(QString status);
	/**
	 * Enable the login button for the login tab in the login/signup popup
	 */
	void loginEnableLoginButton();
	/**
	 * Disable the login button for the login tab in the login/signup popup
	 */
	void loginDisableLoginButton();
	/**
	 * Set the username input field for the login tab in the login/signup popup (used for autofill username)
	 * @param username The username
	 */
	void loginSetUsername(QString username);

	// Signup

	/**
	 * Set the status for the signup tab in the login/signup popup
	 * @param status The status message
	 */
	void signupSetStatus(QString status);
	/**
	 * Disable the register button for the signup tab in the login/signup popup
	 */
	void signupEnableRegisterButton();
	/**
	 * Disable the register button for the signup tab in the login/signup popup
	 */
	void signupDisableRegisterButton();

	// Footer

	/**
	 * Set the status in the footer
	 * @param status The new status
	 */
	void footerSetStatus(QString status);
	/**
	 * Set the error in the footer
	 * @param error The error message
	 */
	void footerSetError(QString error);

	// Notifications

	/**
	 * Add a new notification to the notifications tab
	 * @param message The new notification message
	 */
	void notification(QString message);
	/**
	 * Dismiss an existing notification in the notifications tab
	 * @param index The index of the notification
	 */
	void dismissNotification(int index);
	/**
	 * Show a desktop notification
	 * @param title The title of the notification
	 * @param message The message of the notification
	 */
	void showDesktopNotification(QString title, QString message);

	// Log

	/**
	 * Add a message to the log
	 * @param logText The message to be added
	 */
	void log(QString logText);

	// QML -> C++
public slots:
	// Main

	/**
	 * Handle the start of the main window
	 *
	 * Check the configuration, load the values to the GUI and start the CLI is needed
	 * Opens the according popup in the window
	 * @param startWithCli Should the CLI be started?
	 */
	void onStart(bool startWithCli);
	/**
	 * Handle the "Switch Server" button
	 *
	 * Disconnect from the server
	 */
	void onSwitchServer();

	// No Config Found Popup

	/**
	 * Handle the continue button after a CLI has been selected
	 *
	 * Creates a default config with the path to the selected CLI and runs the onStart method
	 * @param cli_path The path to the CLI
	 */
	void onNoConfigFoundPopupContinueButton(QString cli_path);

	// Invalid Cli Path Popup

	/**
	 * Handle the continue button after a CLI has been selected
	 *
	 * Sets the new CLI path in the config and runs the onStart method
	 * @param cli_path The path to the CLI
	 */
	void onInvalidCliPathPopupContinueButton(QString cli_path);
	/**
	 * Handle the quit button
	 *
	 * Emits a signal to close the main window
	 */
	void onInvalidCliPathPopupQuitButton();

	// Invalid Config Popup

	/**
	 * Handle the "create default" button
	 *
	 * Sets up a default config and opens the "invalid cli path" popup so that the user can select a CLI
	 */
	void onInvalidConfigPopupCreateDefaultButton();
	/**
	 * Handle the quit button
	 *
	 * Emits a signal to close the main window
	 */
	void onInvalidConfigPopupQuitButton();

	// Server Files

	/**
	 * Handle the selection of a file with the file dialog
	 *
	 * Sets the text for the selected file and enables the "send file" button
	 * @param url The new file url
	 */
	void onServerFilesSelectFileButton(QUrl url);
	/**
	 * Handle the "send file" button
	 *
	 * Upload the selected file to the server
	 */
	void onServerFilesSendFileButton();
	/**
	 * Handle the "clear selection" button
	 *
	 * Resets the selected file
	 */
	void onServerFilesClearSelectionButton();
	/**
	 * Handle the download button for a file
	 *
	 * Downloads the file from the server
	 * @param fileName The name of the file
	 */
	void onServerFilesDownloadFileButton(QString fileName);
	/**
	 * Handle the delete button in the delete confirmation popup
	 *
	 * Deletes the file from the server
	 * @param filename The name of the file
	 */
	void onServerFilesConfirmDeleteFileButton(QString fileName);
	/**
	 * Handle the queue button for a file
	 *
	 * Puts the file into the queue on the server
	 * @param fileName The name of the file
	 */
	void onReceivingQueueFileButton(QString fileName);
	/**
	 * Handle the queue button for a file
	 *
	 * Dequeues the file on the server
	 * @param fileName The name of the file
	 */
	void onReceivingDequeueFileButton(QString fileName);

	// Settings

	/**
	 * Handle the selection of a keyfile with the file dialog
	 *
	 * Selects the new keyfile on the CLI for further use
	 * @param path The path to the keyfile
	 */
	void onKeyfileSelected(QString path);
	/**
	 * Handle the "close keyfile" button
	 *
	 * Closes the keyfile on the CLi
	 */
	void onKeyfileClosed();
	/**
	 * Handle the delete button in the delete me confirmation popup
	 *
	 * Tries to delete the current user from the server when the password is correct
	 * @param password The entered password
	 */
	void onSettingsDeleteMeButton(QString password);
	/**
	 * Handle the "revert changes" button
	 *
	 * Sets the settings values to those in the config file
	 */
	void onSettingsRevertChangesButton();
	/**
	 * Handle the reset button
	 *
	 * Sets the settings to the default values
	 */
	void onSettingsResetButton();
	/**
	 * Handle the save button
	 *
	 * Saves the current values of the settings in the GUI to the config file
	 * @param saveIP Should the autofill ip toggle be switched on or off?
	 * @param saveUsername Should the autofill username toggle be switched on or off?
	 * @param cliPath The path to the cli
	 * @param keyPath The path to the keyfile
	 */
	void onSettingsSaveButton(bool saveIP, bool saveUsername, QString cliPath, QString keyPath);

	// Ip Popup

	/**
	 * Handle the connect button
	 *
	 * Connects to the server and saves the ip in the config if desired
	 * @param ip The entered ip address
	 * @param saveAsDefault Was the "Save IP" checkbox ticked?
	 */
	void onIpPopupConnectButton(QString ip, bool saveAsDefault);

	// Login

	/**
	 * Handle the login button
	 *
	 * Logs in on the server and saves the username in the config if desired
	 * @param username The entered username
	 * @param password The entered password
	 * @param saveAsDefault Was the "Save Username" checkbox ticked?
	 */
	void onLoginLoginButton(QString username, QString password, bool saveAsDefault);

	// Signup

	/**
	 * Handle the register button
	 *
	 * Registers on the server and saves the username in the config if desired
	 * @param username The entered username
	 * @param passwordOne The entered first password
	 * @param passwordTwo The entered confirmation password
	 * @param saveAsDefault Was the "Save Username" checkbox ticked?
	 */
	void onSignupRegisterButton(QString username, QString passwordOne, QString passwordTwo, bool saveAsDefault);

	// Notifications

	/**
	 * Handle the "x" button on a noticiation
	 *
	 * Dismiss the notification in the notifications tab
	 * @param id The id of the notification
	 */
	void onDismissNotificationButton(int id);
};

#endif // QMLHANDLER_H
