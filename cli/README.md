# Using the Command Line Interface (CLI)

## Starting the CLI

The command line interface can be started with the following arguments:

### Choosing the CLI mode

<b><i>no argument</i></b>:  <br/>
Regular user mode, designed for user interaction.

`--machine`: <br/>
Machine mode, not designed for user interaction. Other programs (like the GUI) can use this for communication with the CLI. For usage, please refer to [GUI-CLI Protocol.md](../../GUI-CLI Protocol.md).

`--batch <file>`: <br/>
Batch mode. A batch file for the CLI must contain commands (as used in regular user mode) separated by line breaks. The operations can be run by passing the path to the batch file as argument <i>&lt;file&gt;</i>.

### Additional arguments

`--usessl <certfile>`: <br/>
Used to enable SSL communication with the server using the certificate file specified by the file path <i>&lt;certfile&gt;</i>. Needs to be passed if server is configured to use SSL encryption. Configurations where only one side has SSL encryption enabled are not supported.

`--verbose`: <br/>
Prints additional output for debugging purposes.


## Connecting to a server and logging in

The user connects to a server using `connect <ip> <port>` where <i>&lt;ip&gt;</i> is the IP of the server and <i>&lt;port&gt;</i> is the port used by the server. The `<port>` parameter is optional. If no port is specified, 1234 is used.

Then a user has to connect by typing `login <username> <password>`. Alternatively the user can create an account and log in with `signup <username> <password>`. If wrong user data was passed, the user is disconnected from the server and needs to connect again.

## Specifying a key file for encryption

The following commands are used to specify key files used for encryption of all sent files and for decryption. They can be used at any time.

`keyfile <filepath>`: <br/>
Set a key file. The path of the file has to be specified by <i>&lt;filepath&gt;</i>.

`closekey`: <br/>
Stop using the previously selected keyfile.

## Viewing information about the connection

`status`: <br/>
This command shows wether the user is connected to a server, and displays the IP and port of the server in this case. If the user is logged in, also the username and information wether a file transfer between server and client is running are shown.

## Interacting with the server

The following commands can be sent when the user is connected to a server and logged in.


`extendedstatus`: <br/>
Shows detailed information about ongoing transfers at the server.

`list`: <br/>
Shows a simple list of the files stored on the server.

`extendedlist`: <br/>
Shows a detailed list of the files stored on the server, containing file size and decryptability.

`notifications`: <br/>
Shows a list of notifications since the last time.
<br/><br/>

`put <filepath>`: <br/>
Request a file upload to the server. The path of the file has to be specified by <i>&lt;filepath&gt;</i>.

`get <filepath>`: <br/>
Request a download of the file with name <i>&lt;filepath&gt;</i> from the server. <br/>
Alternatively a path can be specified where the file with corresponding name is to save, e.g. `get ./subfolder/foo.txt` downloads the file <i>foo.txt</i> from the server to the directory <i>./subfolder/</i>.

`deletefile <filename>`: <br/>
Request a deletion of the file with name <i>&lt;filename&gt;</i> from the server. Has to be activated in the server configuration.

`queue <filename>`: <br/>
To add a file that is already on the server to the queue for sending with the covert channel.

`dequeue <filename>`: <br/>
To remove a file from the queue for sending with the covert channel. If the transfer is already running and the covert cahnnel method allows it, this cancels the transfer.

`head <filename>`: <br/>
Request the first 32 bytes of a file on the server in base64 encoding. If the file is smaller than 32 bytes, the first 4 bytes are shown if possible.
<br/><br/>

`deleteme <password>`: <br/>
Allows the logged in user to delete their account on the server. This action needs to be confirmed with the password.


## Disconnecting and exiting

Disconnecting from a server is possible by typing `disconnect`. After this, it is possible to connect to a server again.

The program can be exited by typing `exit` at any time.


## Displaying a list of commands

`help` can be used at any time and shows a list of commands.

# Example usage scenario
```
user@pc:~/ccats/build$ ./bin/ccats-cli
machine mode is 0 file is  enablessl is 0 verbose is 0

Please connect to a server via "connect <ip> <port>", then
login by entering "login <username> <password>"
or sign up and log in with "signup <username> <password>".

ccats> connect 0.0.0.0
Version check ok.
ccats> login user pass
Login ok.
ccats> keyfile key.bin
Using keyfile key.bin.
ccats> status
user@0.0.0.0:1234 - Status: ok
ccats> put ./loremipsum.txt
Begin uploading file loremipsum.txt
ccats> extendedlist
Files stored on server:
size (kBytes)  decryptable  file
         0.51  unknown      loremipsum3.txt
       339.98  yes          loremipsum.txt
         0.74  plaintext    loremipsum4.txt
       339.98  yes          somefancyfile.html
         0.60  no           loremipsum2.txt
ccats> queue loremipsum.txt
File loremipsum.txt queued succesfully.
ccats> queue loremipsum4.txt
File loremipsum4.txt queued succesfully.
ccats> extendedstatus

Transfers between different servers:
type      progress  method               bytes/sec  file
upload         42%  tcpurgency             9000.01  loremipsum.txt
queued          0%  tcpurgency                0.00  loremipsum4.txt

ccats> get ./subpath/somefancyfile.html
Begin downloading file somefancyfile.html.
ccats> exit
Disconnect successful.
done
```

### Batchfile example

The same result as above can be produced by writing all commands into a batchfile:
```
connect 0.0.0.0
login user pass
keyfile key.bin
status
put ./loremipsum.txt
extendedlist
queue loremipsum.txt
queue loremipsum4.txt
extendedstatus
get ./subpath/somefancyfile.html
exit
```
After calling `./bin/ccats-cli --batch batchfile.bat` these get executed and the same output from above can be found in `batchfile.bat.out`. If an error occurs, the execution is cancelled and the error can be found in `batchfile.bat.err`.
