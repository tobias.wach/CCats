#ifndef BASE64_H
#define BASE64_H

#include <string>
#include <vector>

namespace base64 {
/**
 * Decodes base64 encoded strings.
 *
 * @param val base64 encoded string
 *
 * @return decoded data
 */
template <typename T> T decode(const std::string &val);

/**
 * Encodes a string to base64.
 *
 * @param val data
 *
 * @return base64 encoded string
 */
template <typename T> std::string encode(const T &val);

} // namespace base64

#endif
