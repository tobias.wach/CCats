#include "../include/Config.h"

namespace Config {
std::map<std::string, std::string> storage;
};

bool Config::init(const std::string &filename) { return true; }

std::string Config::getValue(const std::string &key) {
	auto it = storage.find(key);
	if (it != storage.end()) {
		return it->second;
	}

	return "";
}
