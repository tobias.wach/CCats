# GUI-CLI Protocol

Protocol version: <b>"0.3"</b>

Every json message must be minimized (i.e. must not contain newlines) and be followed by a newline. This rule makes receiving and parsing the messages easier because you can read until a newline comes and you will parse every json message seperately because you won't read multiple json messages from the buffer at a time.

## 0. Connect to the server

The GUI creates a new instance of the CLI with parameter `--machine`. Then the GUI sends a connect command to connect to a server. 

## 1. Start a connection

### 1.1 Connecting and version check

The CLI then tries to establish a connection to the given server and tells the GUI if the connection was successful.

GUI:
```
write: "connect" ip_address port
```
Alternatively, it is possible to only pass an ip address, then by default port 1234 is used.


CLI:
```
{
	"command": "connect",
	"accept": bool,
	"error": string
}
```

To connect to a server, the CLI has to do a version check. Once that is successful, the GUI will pass the username and password to establish a connection.

CLI:
```
{
	"command": "version",
	"serverversion": string,
	"clientversion": string,
	"accept": bool
}
```

If `accept` is `true` the connection is valid. Else the connection will be terminated after the server answered. In this case it it possible to connect again (possibly to another server).


## 1.2 Login / Signup

The GUI will send a login or signup request into the pipe. If the response contains a "true" for accept, the login was successful.

### 1.2.1 Login
GUI:
```
write: "login" username password
```

CLI:
```
{
	"command": "login",
	"accept": bool,
	"error": string
}
```

If `accept` is `true` the connection is valid and the user is logged in. Else `error` has an error string and the connection to the server will be terminated after the server answered.

### 1.2.2 Signup
GUI:
```
write: "signup" username new_password
```

CLI:
```
{
	"command": "signup",
	"accept": bool,
	"error": string
}
```

If `accept` is `true` the connection is valid and the user is logged in and signed up. Else `error` has an error string and the connection to the server will be terminated after the server answered.


## 2. Sending commands

Commands can be sent by the client after a connection has been negotiated (See 1).
Commands can be used unrelated to each other.

### 2.1 Status command
GUI:
```
write: "status"
```

CLI:
```
{
	"command": "status",
	"response": string
}
```

### 2.2 List command
`list` to request a list download.

#### 2.2.1 list - request file list download
GUI:
```
write: "list"
```
CLI:
```
{
	"command": "list",
	"accept": bool,
	"names": string[],
	"error": string
}
```


### 2.3 Put command

Put is split in two commands. `put` to request a file upload and `putdata` to get the information about an ongoing upload.

#### 2.3.1 put - request upload
GUI:
```
write: "put" file_path
```

CLI:
```
{
	"command": "put",
	"accept": bool,
	"file": string,
	"error": string
}
```
If `accept` is `true` the connection is valid and the client can start sending the file. Else the put request was rejected and is hereby canceled. `error` should contain an error string if the request was rejected.


#### 2.3.2 putdata - upload data
CLI:
```
{
	"command": "putdata",
	"file": string,
	"cancel": bool,
	"error": string
}
```
If `cancel` is `true` then the upload of the file is canceled and an error message should be in `error`.


### 2.4 Get command

Get is split in two commands. `get` to request a file download and `getdata` to get the information about an ongoing download.

#### 2.4.1 get - request download

GUI:
```
write: "get" file_path
```

CLI:
```
{
	"command": "get",
	"accept": bool,
	"file": string,
	"error": string
}
```
If `accept` is `true` the connection is valid and the server can start sending the file. Else the get request was rejected and is hereby canceled. `error` should contain an error string if the request was rejected.

#### 2.4.2 getdata - download data

CLI:
```
{
	"command": "getdata",
	"file": string,
	"cancel": bool,
	"error": string
}
```

If `cancel` is `true` then the download of the file is canceled and an error message should be in `error`.

### 2.5 Deletefile command

Requests the deletion of a file from the server.

GUI:
```
write: "deletefile" file_name
```
CLI:
```
{
	"command": "deletefile",
	"file": string,
	"accept": bool,
	"error": string
}
```
If `accept` is `true` the command is valid and the server has deleted the file. Else the request was rejected no changes have been made. `error` should contain an error string in that case.

### 2.6 Deleteme command

The `deleteme` command allows a logged in user to delete their account on the server. This action needs to be confirmed with the users password.

GUI:
```
write: "deleteme" password
```
CLI:
```
{
	"command": "deleteme",
	"accept": bool,
	"error": string
}
```
If `accept` is true the user has been deleted and the connection will be closed by the server.

### 2.7 Notifications

The `notifications` command allows the logged in user to get a list of notifications since the last time.

GUI:
```
write: "notifications"
```

CLI:
```
{
	"command": "notifications",
	"accept": bool,
	"messages": string[],
	"error": string
}
```

### 2.8 ExtendedStatus

The `extendedstatus` command allows the client to request detailed information about ongoing transfers at the server.

GUI:
```
write: "extendedstatus"
```

CLI:
```
{
    "command": "extendedstatus",
    "accept": bool,
    "error": string,
    "transfersclientserver": {
        "upload": bool,
        "file": string,
        "progress": float
    }[],
    "transfersserverserver": {
        "type": string,
        "file": string,
        "progress": float,
        "speed": float,
        "method": string
    }[]
}
```
The answer consists of an array of objects, each representing a transfer. <br />
In case of client-server transfers, `upload` indicates wether the transfer is an up- or download. In case of server-server transfers, `type` indicates wether the file  is `upload`ing, `download`ing or `queued` for upload. <br />
`file` contains the name of the file being transferred. <br />
`progress` contains the percentage completed. <br />
`speed` contains the speed in bytes per second. <br />
`method` contains the covert channel method being used.

### 2.9 Queue command

To add a file that is already on the server to the queue for sending with the covert channel, the use the `queue` command.

Client:
```
write: "queue" file_name
```

CLI:
```
{
	"command": "queue",
	"file": string,
	"accept": bool,
	"error": string
}
```

### 2.10 Dequeue command

To remove a file from the queue for sending with the covert channel, the use the `dequeue` command.

Client:
```
write: "dequeue" file_name
```

CLI:
```
{
	"command": "dequeue",
	"file": string,
	"accept": bool,
	"error": string
}
```

### 2.11 Extendedlist command

GUI:
```
write: "extendedlist"
```
CLI:
```
{
	"command": "extendedlist",
	"accept": bool,
	"files": {
        "name": string,
        "encrypted": string,
        "size": float
    }[],
	"error": string
}
```

The list contains the `name` of a file and its `size` in kByte. <br />
The `encrypted` field contains information wether the file is `unknown` in case of bad signature or file size, `unencrypted`, `decryptable` or `undecryptable`.

## 3. Disconnecting and exiting

### 3.1. Disconnect

GUI:
```
write: "disconnect"
```

CLI:
```
{
	"command": "disconnect",
	"accept": bool
}
```

If `accept` is true, the connection is closed and it is possible to connect to another server.


### 3.2. Exit

GUI:
```
write: "exit"
```

If the CLI is not connected, the program can exit directly. Otherwise, the CLI sends a disconnect reply first: 

CLI:
```
{
	"command": "disconnect",
	"accept": bool
}
```

If `accept` is true, the connection is closed and the program can exit.

## 4. Errors

If an unexpected error occurs, the GUI might receive an `error` message:
```
{
	"command": "error",
	"error": string
}
```
If the server does not respond to requests for some time, the GUI receives a `connectionerror` message and the the connection to the server is terminated:
```
{
	"command": "connectionerror",
	"error": string
}
```
