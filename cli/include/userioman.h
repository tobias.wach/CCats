#ifndef USERIOMAN_H
#define USERIOMAN_H

#include "ioman.h"

/**
 * @class UserIoMan
 *
 * Provides specific implementations of IoMan outputs and prompts
 * for interactive user sessions
 */
class UserIoMan : public IoMan {
private:
	/**
	 * Map containing pointers to the appropriate member functions for printing
	 * formatted json output
	 */
	map<string, void (UserIoMan::*)(Json::Value)> printmap;

	/**
	 * Class-wide json functionality
	 */
	Json::CharReader *reader;
	Json::StreamWriterBuilder wbuilder;
	string jsonerror;

	bool verbose;

	/**
	 * Format and pretty print json for terminal output
	 */
	void printJson(Json::Value root);

	/**
	 * Method prototypes for printing json output
	 */
	/* printing commands go here */
	void printError(Json::Value root);
	void printConnect(Json::Value root);
	void printHelp(Json::Value root);
	void printStatus(Json::Value root);
	void printExtendedstatus(Json::Value root);
	void printDisconnect(Json::Value root);
	void printPut(Json::Value root);
	void printGet(Json::Value root);
	void printList(Json::Value root);
	void printExtendedlist(Json::Value root);
	void printVersion(Json::Value root);
	void printLogin(Json::Value root);
	void printSignup(Json::Value root);
	void printPutdata(Json::Value root);
	void printGetdata(Json::Value root);
	void printListdata(Json::Value root);
	void printHead(Json::Value root);
	void printDeletefile(Json::Value root);
	void printQueue(Json::Value root);
	void printDequeue(Json::Value root);
	void printDeleteme(Json::Value root);
	void printKeyfile(Json::Value root);
	void printClosekey(Json::Value root);
	void printNotifications(Json::Value root);

	/**
	 * Mutex for synchronized message output
	 */
	std::mutex msgmutex;

public:
	/**
	 * Constructor and destructor
	 */
	UserIoMan(bool usessl, const char *certfile, bool verbose);
	~UserIoMan();

protected:
	/**
	 * Specific implementations for printing messages
	 */
	void printMessage(std::string msg, OutMsgType type);
	void printWelcomeMessage();

	/**
	 * Return the specific prompt strings for IoMan prompts
	 */
	std::string getCmdPrompt();
};

#endif
