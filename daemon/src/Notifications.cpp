#include "../include/Notifications.h"

#include <ctime>

namespace Notifications {
std::vector<std::pair<long int, std::string>> messages;
std::map<std::string, long int> userTimeStamps;
}; // namespace Notifications

void Notifications::newNotification(const std::string &message) {
	long int timestamp = static_cast<long int>(time(0));
	messages.push_back(make_pair(timestamp, message));
}

std::vector<std::string> Notifications::getMessages(const std::string &user) {
	std::vector<std::string> ret;

	// first clean up
	long int latest = static_cast<long int>(time(0)) - 604800;
	for (int i = 0; i < messages.size(); i++) {
		if (messages.at(i).first < latest) {
			messages.erase(messages.begin() + i);
		}
	}

	auto it = userTimeStamps.find(user);
	if (it == userTimeStamps.end()) {
		// case user has no timestamp yet

		for (int i = 0; i < messages.size(); i++) {
			ret.push_back(messages.at(i).second);
		}
		userTimeStamps.insert(std::pair<std::string, long int>(user, static_cast<long int>(time(0))));
	} else {
		long int userTimeStamp = it->second;

		for (int i = 0; i < messages.size(); i++) {
			if (userTimeStamp <= messages.at(i).first) {
				ret.push_back(messages.at(i).second);
			}
		}

		// update user timestamp
		it->second = static_cast<long int>(time(0));
	}

	return ret;
}
