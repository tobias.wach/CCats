#!/bin/bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then
    cd "$SCRIPT_PATH/.."
fi

DAEMONFILEPATH="../../daemon/build/files"

if [ $# -eq 1 ];
then
	DAEMONFILEPATH="$1"
fi

echo "am in directory $PWD"
echo "using file path $DAEMONFILEPATH"

bin/ccats-cli --batch test/cryptoput.txt

if [ ! -f test/cryptoput.txt.out ];
then
	echo "running of batch file failed"
	exit 1;
fi

if [ ! -f $DAEMONFILEPATH/samplefile.txt ];
then
	echo "couldnt find file on server"
	echo "STDOUT is :"
	cat test/cryptoput.txt.out
	echo "STDERR is :"
	cat test/cryptoput.txt.err
	exit 1;
fi

test/cryptotest_gcm 0 test/samplekey1.bin $DAEMONFILEPATH/samplefile.txt samplefile.txt
CRYPTORES=$?

diff samplefile.txt test/samplefile.txt
DIFFRES=$?

rm $DAEMONFILEPATH/samplefile.txt
rm samplefile.txt

if [ $CRYPTORES -ne 0 ];
then
	echo "decryption failed: $CRYPTORES"
	exit 1;
fi

if [ $DIFFRES -ne 0 ];
then
	echo "files are not equal after decryption: $DIFFRES"
	exit 1;
fi

exit 0;
