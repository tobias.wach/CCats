# Covert Protocol
The Covert Protocol is the protocol used to communicate over the covert channel. The communication over the covert channels aims mainly on file transfers.

# Bidirectional Covert Protocol
A bidirectional communication which uses segment numbers to prevent data loss.

A packet is at least 2 bytes big. The first byte is the header followed by one or multiple data bytes.

```
|   1 byte   |       n bytes       |
|   header   |        data         |
```



## Header
A header contains a command and a segment number. Even if the `no operation` command is used the segment number must be counted.

```
|          8 bit          |
|          header         |

| 2 bit |  5 bit  | 1 bit |
|  seg  |    -    | reset |
```


### Segment
The segment number is a counter for the packets in the covert channels. The segment number is increased on the passive server side.

An example how the packet exchange might look like.
```
A                       B
seg                   seg
=========================
1 -------------------->
  <-------------------- 2
2 -------------------->
  <-------------------- 3
3 --------- X                Packet lost

3 -------------------->      Retransmission from A
  <-------------------- 4
4 -------------------->
            X --------- 5    Packet lost

            X --------- 5    Retransmission from B
4 --------------------> 4    Retransmission from A
  <-------------------- 5
```

### Reset
A reset will will be triggered by a set reset bit.
Running file transfers will be canceled and the segment counter will be reset to its initial value.

## Data
The data will be transmitted over a sequential protocol. You could in fact use the data protocol and ditch the bidirectional protocol.
There are several states of a transfer:

0. idle - does nothing
1. file name size - sends size of file name as 1 byte unsigned int
2. file name - sends file name chars and loops until the full file name is transmitted
3. data size - sends 4 bytes of data size and loops until the full data size is transmitted
4. data - sends n bytes of data and loops until the full file is transmitted
