import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import Qt.labs.platform 1.1

Page {
    width: 1280
    height: 570
    id: notificationsForm

    font.capitalization: Font.MixedCase

    Connections {
        target: _qmlHandler
        onNotification: {
            notificationList.append({
                                        "notificationDate": new Date().toLocaleString(
                                                                Qt.locale(
                                                                    "de_DE"),
                                                                "[dd.MM.yyyy hh:mm:ss]"),
                                        "notificationMessage": message
                                    })
        }

        onDismissNotification: {
            notificationList.remove(index)
        }

        onShowDesktopNotification: {
            trayIcon.showMessage(title, message)
        }
    }

    ColumnLayout {
        anchors.fill: parent

        ScrollView {
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 370

            ListView {
                anchors.fill: parent
                model: notificationList
                clip: true

                delegate: NotificationTemplate {
                    notificationDateText: notificationDate
                    notificationMessageText: notificationMessage
                    myIndex: index
                }
            }
        }

        ListModel {
            id: notificationList
        }

        Text {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 30
            id: loginTitle

            text: qsTr("No new notifications!")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            visible: notificationList.count == 0 ? true : false
            font.pixelSize: 20
        }

        Button {
            id: notificationsDismissAllButton
            Layout.preferredWidth: 180
            Layout.preferredHeight: 70
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Dismiss all")
            enabled: notificationList.count != 0 ? true : false
            visible: enabled

            // @disable-check M223
            onClicked: {
                // @disable-check M222
                notificationList.clear()
            }
        }
    }

    SystemTrayIcon {
        id: trayIcon
        visible: true
    }
}
