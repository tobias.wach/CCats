import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Popup {
    id: popup
    height: 450
    dim: true
    clip: false
    width: 400
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    signal resetOnClose

    onClosed: {
        resetOnClose()
    }

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    Connections {
        target: _qmlHandler
        onLoginSignupPopupClose: {
            popup.close()
        }
        onLoginSignupPopupOpen: {
            popup.open()
        }

        onLoginSetUsername: {
            loginUsernameInput.text = username
        }
        onLoginSetStatus: {
            loginStatusText.text = status
        }
        onLoginEnableLoginButton: {
            loginLoginButton.enabled = true
        }
        onLoginDisableLoginButton: {
            loginLoginButton.enabled = false
        }
        onLoginSignupCheckSaveCheckbox: {
            loginSetDefaultCheckbox.checked = true
            signupSetDefaultCheckbox.checked = true
        }

        onSignupSetStatus: {
            signupStatusText.text = status
        }
        onSignupEnableRegisterButton: {
            signupRegisterButton.enabled = true
        }
        onSignupDisableRegisterButton: {
            signupRegisterButton.enabled = false
        }
    }

    Page {
        anchors.fill: parent

        header: TabBar {
            id: header
            currentIndex: swipeView.currentIndex
            contentHeight: 50

            TabButton {
                text: qsTr("Login")
            }

            TabButton {
                text: qsTr("Signup")
            }
        }

        SwipeView {
            id: swipeView
            anchors.fill: parent
            currentIndex: header.currentIndex
            clip: true

            // Login
            Page {
                width: 400
                height: 400
                title: ""

                ColumnLayout {
                    anchors.fill: parent

                    Text {
                        Layout.alignment: Qt.AlignCenter
                        id: loginTitle

                        text: qsTr("Login")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 20
                    }

                    TextField {
                        Layout.alignment: Qt.AlignCenter
                        id: loginUsernameInput
                        selectByMouse: true
                        focus: true
                        text: qsTr("")
                        placeholderText: "Username"
                        horizontalAlignment: Text.AlignHCenter
                        // @disable-check M222
                        Keys.onReturnPressed: loginLoginButton.activate()
                        // @disable-check M222
                        Keys.onEnterPressed: loginLoginButton.activate()

                        onTextEdited: loginLoginButton.enabled
                                      = (loginUsernameInput.text != ""
                                         && loginPasswordInput.text != "")

                        Connections {
                            target: popup
                            onResetOnClose: loginUsernameInput.text = ""
                        }
                    }

                    TextField {
                        Layout.alignment: Qt.AlignCenter
                        id: loginPasswordInput
                        selectByMouse: true
                        text: qsTr("")
                        placeholderText: "Password"
                        horizontalAlignment: Text.AlignHCenter
                        // @disable-check M222
                        Keys.onReturnPressed: loginLoginButton.activate()
                        // @disable-check M222
                        Keys.onEnterPressed: loginLoginButton.activate()
                        echoMode: TextInput.Password

                        onTextEdited: loginLoginButton.enabled
                                      = (loginUsernameInput.text != ""
                                         && loginPasswordInput.text != "")

                        Connections {
                            target: popup
                            onResetOnClose: loginPasswordInput.text = ""
                        }
                    }

                    CheckBox {
                        id: loginSetDefaultCheckbox
                        Layout.alignment: Qt.AlignCenter
                        checked: false
                        text: "Save as default user"

                        Connections {
                            target: popup
                            onResetOnClose: loginSetDefaultCheckbox.checked = false
                        }
                    }

                    Text {
                        id: loginStatusText
                        color: "#df3f3f"
                        text: qsTr("")
                        wrapMode: Text.WordWrap
                        Layout.preferredWidth: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        Layout.alignment: Qt.AlignCenter
                        font.pixelSize: 20

                        Connections {
                            target: popup
                            onResetOnClose: loginStatusText.text = ""
                        }
                    }

                    Button {
                        Layout.alignment: Qt.AlignCenter
                        id: loginLoginButton
                        text: qsTr("Login")
                        enabled: false
                        font.pointSize: 16
                        // @disable-check M223
                        onClicked: {
                            // @disable-check M222
                            loginLoginButton.activate()
                        }

                        // @disable-check M222
                        function activate() {
                            // @disable-check M223
                            if (loginLoginButton.enabled) {
                                // @disable-check M222
                                _qmlHandler.onLoginLoginButton(
                                            loginUsernameInput.text,
                                            loginPasswordInput.text,
                                            loginSetDefaultCheckbox.checked)
                            }
                        }
                    }
                }
            }

            // Signup
            Page {
                width: 400
                height: 400

                ColumnLayout {
                    anchors.fill: parent

                    Text {
                        Layout.alignment: Qt.AlignCenter
                        id: signupTitle

                        text: qsTr("Signup")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 20
                    }

                    TextField {
                        Layout.alignment: Qt.AlignCenter
                        id: signupUsernameInput
                        selectByMouse: true
                        focus: true
                        text: qsTr("")
                        placeholderText: "Username"
                        horizontalAlignment: Text.AlignHCenter
                        // @disable-check M222
                        Keys.onReturnPressed: signupRegisterButton.activate()
                        // @disable-check M222
                        Keys.onEnterPressed: signupRegisterButton.activate()

                        onTextEdited: {
                            signupStatusText.text = ""
                            signupRegisterButton.enabled
                                    = (signupUsernameInput.text != ""
                                       && signupPasswordOneInput.text != ""
                                       && signupPasswordTwoInput.text != "")
                        }

                        Connections {
                            target: popup
                            onResetOnClose: signupUsernameInput.text = ""
                        }
                    }

                    TextField {
                        Layout.alignment: Qt.AlignCenter
                        id: signupPasswordOneInput
                        selectByMouse: true
                        focus: true
                        text: qsTr("")
                        placeholderText: "Password"
                        horizontalAlignment: Text.AlignHCenter
                        // @disable-check M222
                        Keys.onReturnPressed: signupRegisterButton.activate()
                        // @disable-check M222
                        Keys.onEnterPressed: signupRegisterButton.activate()
                        echoMode: TextInput.Password

                        onTextEdited: {
                            signupStatusText.text = ""
                            signupRegisterButton.enabled
                                    = (signupUsernameInput.text != ""
                                       && signupPasswordOneInput.text != ""
                                       && signupPasswordTwoInput.text != "")
                        }

                        Connections {
                            target: popup
                            onResetOnClose: signupPasswordOneInput.text = ""
                        }
                    }

                    TextField {
                        Layout.alignment: Qt.AlignCenter
                        id: signupPasswordTwoInput
                        selectByMouse: true
                        focus: true
                        text: qsTr("")
                        placeholderText: "Repeat Passw."
                        horizontalAlignment: Text.AlignHCenter
                        // @disable-check M222
                        Keys.onReturnPressed: signupRegisterButton.activate()
                        // @disable-check M222
                        Keys.onEnterPressed: signupRegisterButton.activate()
                        echoMode: TextInput.Password

                        onTextEdited: {
                            signupStatusText.text = ""
                            signupRegisterButton.enabled
                                    = (signupUsernameInput.text != ""
                                       && signupPasswordOneInput.text != ""
                                       && signupPasswordTwoInput.text != "")
                        }

                        Connections {
                            target: popup
                            onResetOnClose: signupPasswordTwoInput.text = ""
                        }
                    }

                    CheckBox {
                        id: signupSetDefaultCheckbox
                        Layout.alignment: Qt.AlignCenter
                        checked: false
                        text: "Save as default user"

                        Connections {
                            target: popup
                            onResetOnClose: signupSetDefaultCheckbox.checked = false
                        }
                    }

                    Text {
                        id: signupStatusText
                        color: "#df3f3f"
                        text: qsTr("")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        Layout.alignment: Qt.AlignCenter
                        wrapMode: Text.WordWrap
                        Layout.preferredWidth: parent.width
                        font.pixelSize: 20

                        Connections {
                            target: popup
                            onResetOnClose: signupStatusText.text = ""
                        }
                    }

                    Button {
                        Layout.alignment: Qt.AlignCenter
                        id: signupRegisterButton
                        text: qsTr("Register")
                        enabled: false
                        font.pointSize: 16
                        // @disable-check M223
                        onClicked: {
                            // @disable-check M222
                            signupRegisterButton.activate()
                        }

                        // @disable-check M222
                        function activate() {
                            // @disable-check M223
                            if (signupRegisterButton.enabled) {
                                // @disable-check M222
                                _qmlHandler.onSignupRegisterButton(
                                            signupUsernameInput.text,
                                            signupPasswordOneInput.text,
                                            signupPasswordTwoInput.text,
                                            signupSetDefaultCheckbox.checked)
                            }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        swipeView.interactive = false
    }
}
